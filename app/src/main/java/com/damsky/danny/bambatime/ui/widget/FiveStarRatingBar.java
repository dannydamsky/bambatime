package com.damsky.danny.bambatime.ui.widget;

import android.content.Context;
import android.support.v7.widget.AppCompatRatingBar;
import android.util.AttributeSet;

/**
 * This View is the same as the normal RatingBar, it is used for a RatingBar with five stars.
 * This View should be used in correspondence with the Movie entity. So even though there are five
 * stars the getRating() and setRating() functions return values from 0 to 10.
 *
 * @author Danny Damsky
 */
public final class FiveStarRatingBar extends AppCompatRatingBar {

    {
        setNumStars(5);
        setStepSize(0.1f);
    }

    public FiveStarRatingBar(Context context) {
        super(context);
    }

    public FiveStarRatingBar(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public FiveStarRatingBar(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    /**
     * @return The number of stars filled (up to five) multiplied by 2 (So then it's up to ten,
     * corresponds with the vote_average property of Movie)
     */
    @Override
    public float getRating() {
        return super.getRating() * 2;
    }

    /**
     * Use this for setting movie ratings using the Movie Entity.
     *
     * @param rating The vote_average of the movie, a double value from 0 to 10.
     */
    public void setRating(double rating) {
        float float_rating = (float) (rating / 2);
        super.setRating(float_rating);
    }

    /**
     * @deprecated Do not use this function from outside of this class!
     * Use setRating(double rating) instead.
     *
     * @param rating The vote_average of the movie, a value from 0 to 10.
     */
    @Override
    public void setRating(float rating) {
        super.setRating(rating / 2);
    }
}
