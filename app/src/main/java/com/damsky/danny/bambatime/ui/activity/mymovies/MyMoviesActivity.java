package com.damsky.danny.bambatime.ui.activity.mymovies;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.FileProvider;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.app.AppCompatDelegate;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.Surface;
import android.view.View;
import android.view.WindowManager;
import android.widget.TextView;
import android.widget.Toast;

import com.damsky.danny.bambatime.App;
import com.damsky.danny.bambatime.R;
import com.damsky.danny.bambatime.data.localdb.model.Movie;
import com.damsky.danny.bambatime.ui.activity.about.AboutPageActivity;
import com.damsky.danny.bambatime.ui.activity.bookmarks.BookmarkedMoviesActivity;
import com.damsky.danny.bambatime.ui.activity.main.MainActivity;
import com.damsky.danny.bambatime.ui.activity.main.adapter.MainRecycleAdapter;
import com.damsky.danny.bambatime.ui.activity.movie.MovieActivity;
import com.damsky.danny.bambatime.ui.activity.prefs.PreferencesActivity;
import com.damsky.danny.bambatime.ui.activity.search.SearchActivity;
import com.damsky.danny.bambatime.ui.dialog.alertdialog.SimpleAlertDialog;
import com.damsky.danny.bambatime.ui.dialog.imagechoose.ImageChooseDialog;
import com.damsky.danny.bambatime.ui.dialog.moviedetails.BundleHolder;
import com.damsky.danny.bambatime.ui.dialog.moviedetails.MovieDetailsDialog;
import com.damsky.danny.bambatime.util.Constants;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * This activity uses the same layout as the MainActivity.
 * The purpose of this activity is to allow the user to add and delete his own movies to
 * and from the database respectively.
 *
 * @author Danny Damsky
 * @see MainActivity
 * @see MainRecycleAdapter
 * @see MovieDetailsDialog
 * @see ImageChooseDialog
 * @see SimpleAlertDialog
 */

public final class MyMoviesActivity extends AppCompatActivity implements
        NavigationView.OnNavigationItemSelectedListener, MainRecycleAdapter.OnCardClickListener,
        MovieDetailsDialog.OnMovieDetailsDialogDismissListener,
        ImageChooseDialog.OnImageChooseDialogDismissListener,
        SimpleAlertDialog.OnAlertDialogDismissListener {

    private List<Movie> movies;
    private App appReference;

    @BindView(R.id.mainRecyclerView)
    RecyclerView mainRecyclerView;
    @BindView(R.id.drawerLayout)
    DrawerLayout drawerLayout;
    @BindView(R.id.navigationDrawer)
    NavigationView navigationDrawer;
    @BindView(R.id.emptyError)
    TextView emptyError;
    @BindView(R.id.container)
    CoordinatorLayout container;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.fab)
    FloatingActionButton fab;

    private MenuItem checkedItem;

    private String keyForBundle;
    private Bundle dialogBundle;
    private String mCurrentPhotoPath;

    private Movie movieToDelete;

    private boolean doubleTapToExit = false;

    private boolean isLandscape() {
        int rotationDegrees = ((WindowManager) Objects.requireNonNull(getSystemService(WINDOW_SERVICE)))
                .getDefaultDisplay()
                .getRotation();

        return rotationDegrees == Surface.ROTATION_90 || rotationDegrees == Surface.ROTATION_270;
    }

    private boolean isLargeScreen() {
        int screenSize = getResources().getConfiguration().screenLayout &
                Configuration.SCREENLAYOUT_SIZE_MASK;

        return screenSize == Configuration.SCREENLAYOUT_SIZE_LARGE ||
                screenSize == Configuration.SCREENLAYOUT_SIZE_XLARGE;
    }

    // Make sure to show the TextView message if the RecyclerView is empty.

    private void updateRecyclerViewAdapter() {
        mainRecyclerView.getAdapter().notifyDataSetChanged();

        if (movies.size() == 0)
            emptyError.setVisibility(View.VISIBLE);
        else
            emptyError.setVisibility(View.GONE);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        appReference = (App) getApplication();
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        fab.setVisibility(View.VISIBLE);
        fab.setOnClickListener(v -> showMovieDetailsDialog());

        setSupportActionBar(toolbar);

        ActionBarDrawerToggle mToggle = new ActionBarDrawerToggle(this, drawerLayout, toolbar,
                R.string.open_nav_drawer, R.string.close_nav_drawer);

        drawerLayout.addDrawerListener(mToggle);
        mToggle.syncState();

        navigationDrawer.setNavigationItemSelectedListener(this);
        navigationDrawer.getMenu().findItem(R.id.action_my_movies).setChecked(true);

        boolean shouldEnableBigLayout = isLandscape() || isLargeScreen();
        if (shouldEnableBigLayout)
            mainRecyclerView.setLayoutManager(new GridLayoutManager(this, 2));
        else
            mainRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        mainRecyclerView.setItemAnimator(new DefaultItemAnimator());

        movies = appReference.getDbHelper().getUserAddedMovies();
        mainRecyclerView.setAdapter(new MainRecycleAdapter(this, movies, shouldEnableBigLayout));

        if (movies.size() == 0)
            emptyError.setVisibility(View.VISIBLE);

        emptyError.setText(R.string.no_movies_found_mymovies);
    }

    @Override
    protected void onPause() {
        if (dialogBundle != null)
            getIntent().putExtra(Constants.EXTRA_BUNDLE, dialogBundle);

        super.onPause();
    }

    @Override
    public void onBackPressed() {
        if (drawerLayout.isDrawerOpen(navigationDrawer))
            drawerLayout.closeDrawer(navigationDrawer);

        else if (doubleTapToExit)
            super.onBackPressed();

        else {
            final Snackbar snackbar = Snackbar
                    .make(container, R.string.double_tap_message, Snackbar.LENGTH_INDEFINITE);
            snackbar.show();
            doubleTapToExit = true;
            new Handler().postDelayed(() -> {
                doubleTapToExit = false;
                snackbar.dismiss();
            }, Constants.BACK_BUTTON_DOUBLE_TAP_INTERVAL);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        MenuItem sortBy = menu.findItem(R.id.action_sort_by);
        sortBy.setVisible(true);

        menu.findItem(R.id.popularity_desc).setVisible(false);
        menu.findItem(R.id.popularity_asc).setVisible(false);

        onOptionsItemSelected(menu.findItem(R.id.rating_desc));
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.rating_desc:
                Collections.sort(movies, (o1, o2) ->
                        Double.compare(o2.getVote_average(), o1.getVote_average()));
                break;
            case R.id.rating_asc:
                Collections.sort(movies, (o1, o2) ->
                        Double.compare(o1.getVote_average(), o2.getVote_average()));
                break;
            case R.id.release_date_desc:
                Collections.sort(movies, (o1, o2) ->
                        o2.getRelease_date().compareTo(o1.getRelease_date()));
                break;
            case R.id.release_date_asc:
                Collections.sort(movies, (o1, o2) ->
                        o1.getRelease_date().compareTo(o2.getRelease_date()));
                break;
            case R.id.title_asc:
                Collections.sort(movies, (o1, o2) ->
                        o1.getTitle().compareTo(o2.getTitle()));
                break;
            case R.id.title_desc:
                Collections.sort(movies, (o1, o2) ->
                        o2.getTitle().compareTo(o1.getTitle()));
                break;
        }

        item.setChecked(true);
        checkedItem = item;
        updateRecyclerViewAdapter();
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_settings:
                Intent startPrefs = new Intent(this, PreferencesActivity.class);
                startPrefs.putExtra(Constants.EXTRA_NIGHT_MODE_CHANGED, false);
                startActivityForResult(startPrefs, Constants.REQUEST_CODE_PREFERENCES);
                break;

            case R.id.action_discover:
                Intent startMain = new Intent(this, MainActivity.class);
                startActivity(startMain);
                finish();
                break;

            case R.id.action_bookmarked:
                Intent startBookmarkedMovies = new Intent(this, BookmarkedMoviesActivity.class);
                startActivity(startBookmarkedMovies);
                finish();
                break;

            case R.id.action_search:
                Intent startSearch = new Intent(this, SearchActivity.class);
                startActivity(startSearch);
                finish();
                break;

            case R.id.action_about:
                Intent startAbout = new Intent(this, AboutPageActivity.class);
                startActivity(startAbout);
                break;
        }

        drawerLayout.closeDrawer(navigationDrawer);
        return true;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case Constants.REQUEST_CODE_PREFERENCES:
                if (data != null && data.getBooleanExtra(Constants.EXTRA_NIGHT_MODE_CHANGED, false)) {
                    AppCompatDelegate.setDefaultNightMode(appReference.getPreferencesHelper().getTheme());
                    this.recreate();
                }
                break;

            case Constants.REQUEST_CODE_MOVIE_ACTIVITY:
                if (movies != null) {
                    int positionInList = data.getIntExtra(Constants.EXTRA_MOVIE_POSITION_IN_LIST, -1);
                    Long movieId = movies.get(positionInList).getId();
                    movies.set(positionInList, appReference.getDbHelper().getMovieById(movieId));
                    updateRecyclerViewAdapter();
                }
                break;

            case Constants.REQUEST_CODE_GALLERY:
                if (data != null)
                    dialogBundle.putString(keyForBundle, getPath(data.getData()));
                showMovieDetailsDialog();
                break;

            case Constants.REQUEST_CODE_CAMERA:
                Bitmap bitmap = BitmapFactory.decodeFile(mCurrentPhotoPath);

                if (bitmap != null)
                    dialogBundle.putString(keyForBundle, mCurrentPhotoPath);

                mCurrentPhotoPath = null;
                showMovieDetailsDialog();
                break;
        }
    }

    @Override
    public void onCardClicked(int position) {
        Movie movieClicked = movies.get(position);
        Intent intent = new Intent(this, MovieActivity.class);
        intent.putExtra(Constants.EXTRA_MOVIE_ID, movieClicked.getId());
        intent.putExtra(Constants.EXTRA_MOVIE_POSITION_IN_LIST, position);
        startActivityForResult(intent, Constants.REQUEST_CODE_MOVIE_ACTIVITY);
    }

    @Override
    public void onCardLongClicked(int position) {
        movieToDelete = movies.get(position);
        SimpleAlertDialog.show(getSupportFragmentManager(), R.string.confirm_action, R.string.confirm_action_delete_movie);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case Constants.REQUEST_CODE_PERMISSIONS_WRITE_EXTERNAL_STORAGE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED)
                    ImageChooseDialog.show(getSupportFragmentManager());
                else
                    showMovieDetailsDialog();
                break;
        }
    }

    @Override
    public void onImageChooseDialogCameraOption() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        // Ensure that there's a camera activity to handle the intent
        if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
            // Create the File where the photo should go
            File photoFile = null;
            try {
                photoFile = createImageFile();
            } catch (IOException ex) {
                // Error occurred while creating the File
                Toast.makeText(this, R.string.error_creating_image, Toast.LENGTH_SHORT).show();
            }
            // Continue only if the File was successfully created
            if (photoFile != null) {
                Uri photoURI = FileProvider.getUriForFile(this,
                        getApplicationContext().getPackageName() + ".fileprovider",
                        photoFile);
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                startActivityForResult(takePictureIntent, Constants.REQUEST_CODE_CAMERA);
            }
        }
    }

    @Override
    public void onImageChooseDialogCancelled() {
        try {
            if (dialogBundle == null)
                dialogBundle = getIntent().getBundleExtra(Constants.EXTRA_BUNDLE);

            showMovieDetailsDialog();
            getIntent().removeExtra(Constants.EXTRA_BUNDLE);
        } catch (Exception e) {
            // This happens when application rotates, this isn't an error for checked cases.
            Log.d("ErrorInMovieActivity", e.getMessage());
        }
    }

    @Override
    public void onImageChooseDialogGalleryOption() {
        Intent pickImageIntent = new Intent(Intent.ACTION_PICK);
        pickImageIntent.setType("image/*");
        startActivityForResult(Intent.createChooser(pickImageIntent, getString(R.string.select_picture)),
                Constants.REQUEST_CODE_GALLERY);
    }

    @Override
    public void onMovieDetailsDialogDismissed(int resultCode, Bundle bundle) {
        switch (resultCode) {
            case Constants.RESULT_CODE_POSTER:
                dialogBundle = bundle;
                keyForBundle = Constants.MOVIE_POSTER_PATH_USER;
                requestPermissionsForGettingImages();
                break;

            case Constants.RESULT_CODE_BACKDROP:
                dialogBundle = bundle;
                keyForBundle = Constants.MOVIE_BACKDROP_PATH_USER;
                requestPermissionsForGettingImages();
                break;

            case Constants.RESULT_CODE_DIALOG_OK:
                BundleHolder bundleHolder = new BundleHolder(bundle);
                Movie movie = bundleHolder.getNewMovie();
                appReference.getDbHelper().insertMovie(movie);
                movies.add(movie);

                onOptionsItemSelected(checkedItem);

                keyForBundle = null;
                dialogBundle = null;
                break;
        }
    }

    @Override
    public void onAlertDialogOkPressed() {
        appReference.getDbHelper().deleteMovie(movieToDelete);
        movies.remove(movieToDelete);
        movieToDelete = null;
        updateRecyclerViewAdapter();
    }

    private void showMovieDetailsDialog() {
        MovieDetailsDialog.show(getSupportFragmentManager(), dialogBundle, null);
    }

    private void requestPermissionsForGettingImages() {
        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {

            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                    Constants.REQUEST_CODE_PERMISSIONS_WRITE_EXTERNAL_STORAGE);
        } else
            ImageChooseDialog.show(getSupportFragmentManager());
    }

    @SuppressLint("SimpleDateFormat")
    private File createImageFile() throws IOException {
        // Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        File storageDir = getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(
                imageFileName,  /* prefix */
                ".jpg",         /* suffix */
                storageDir      /* directory */
        );

        // Save a file: path for use with ACTION_VIEW intents
        mCurrentPhotoPath = image.getAbsolutePath();
        return image;
    }

    private String getPath(Uri uri) {
        String column = MediaStore.Images.Media.DATA;
        String[] projection = {column};

        Cursor cursor = getContentResolver().query(uri, projection, null, null, null);
        if (cursor != null) {
            int column_index = cursor.getColumnIndex(MediaStore.Images.Media.DATA);
            cursor.moveToNext();
            String path = cursor.getString(column_index);
            cursor.close();
            return path;
        }

        return null;
    }
}
