package com.damsky.danny.bambatime.data.localdb.model;

import android.database.Cursor;
import android.database.sqlite.SQLiteStatement;

import org.greenrobot.greendao.AbstractDao;
import org.greenrobot.greendao.Property;
import org.greenrobot.greendao.database.Database;
import org.greenrobot.greendao.database.DatabaseStatement;
import org.greenrobot.greendao.internal.DaoConfig;
import org.greenrobot.greendao.query.Query;
import org.greenrobot.greendao.query.QueryBuilder;

import java.util.List;

// THIS CODE IS GENERATED BY greenDAO, DO NOT EDIT.

/**
 * DAO for table "GENRES".
 */
public class GenreDao extends AbstractDao<Genre, Long> {

    public static final String TABLENAME = "GENRES";

    /**
     * Properties of entity Genre.<br/>
     * Can be used for QueryBuilder and for referencing column names.
     */
    public static class Properties {
        public final static Property Id = new Property(0, Long.class, "id", true, "_id");
        public final static Property TmdbId = new Property(1, Long.class, "tmdbId", false, "TMDB_ID");
        public final static Property Name = new Property(2, String.class, "name", false, "NAME");
    }

    private DaoSession daoSession;

    private Query<Genre> movie_GenresQuery;

    public GenreDao(DaoConfig config) {
        super(config);
    }
    
    public GenreDao(DaoConfig config, DaoSession daoSession) {
        super(config, daoSession);
        this.daoSession = daoSession;
    }

    /**
     * Creates the underlying database table.
     */
    public static void createTable(Database db, boolean ifNotExists) {
        String constraint = ifNotExists ? "IF NOT EXISTS ": "";
        db.execSQL("CREATE TABLE " + constraint + "\"GENRES\" (" + //
                "\"_id\" INTEGER PRIMARY KEY AUTOINCREMENT ," + // 0: id
                "\"TMDB_ID\" INTEGER UNIQUE ," + // 1: tmdbId
                "\"NAME\" TEXT NOT NULL );"); // 2: name
    }

    /** Drops the underlying database table. */
    public static void dropTable(Database db, boolean ifExists) {
        String sql = "DROP TABLE " + (ifExists ? "IF EXISTS " : "") + "\"GENRES\"";
        db.execSQL(sql);
    }

    @Override
    protected final void bindValues(DatabaseStatement stmt, Genre entity) {
        stmt.clearBindings();
 
        Long id = entity.getId();
        if (id != null) {
            stmt.bindLong(1, id);
        }
 
        Long tmdbId = entity.getTmdbId();
        if (tmdbId != null) {
            stmt.bindLong(2, tmdbId);
        }
        stmt.bindString(3, entity.getName());
    }

    @Override
    protected final void bindValues(SQLiteStatement stmt, Genre entity) {
        stmt.clearBindings();
 
        Long id = entity.getId();
        if (id != null) {
            stmt.bindLong(1, id);
        }
 
        Long tmdbId = entity.getTmdbId();
        if (tmdbId != null) {
            stmt.bindLong(2, tmdbId);
        }
        stmt.bindString(3, entity.getName());
    }

    @Override
    protected final void attachEntity(Genre entity) {
        super.attachEntity(entity);
        entity.__setDaoSession(daoSession);
    }

    @Override
    public Long readKey(Cursor cursor, int offset) {
        return cursor.isNull(offset + 0) ? null : cursor.getLong(offset + 0);
    }    

    @Override
    public Genre readEntity(Cursor cursor, int offset) {
        Genre entity = new Genre( //
                cursor.isNull(offset + 0) ? null : cursor.getLong(offset + 0), // id
                cursor.isNull(offset + 1) ? null : cursor.getLong(offset + 1), // tmdbId
                cursor.getString(offset + 2) // name
        );
        return entity;
    }
     
    @Override
    public void readEntity(Cursor cursor, Genre entity, int offset) {
        entity.setId(cursor.isNull(offset + 0) ? null : cursor.getLong(offset + 0));
        entity.setTmdbId(cursor.isNull(offset + 1) ? null : cursor.getLong(offset + 1));
        entity.setName(cursor.getString(offset + 2));
     }
    
    @Override
    protected final Long updateKeyAfterInsert(Genre entity, long rowId) {
        entity.setId(rowId);
        return rowId;
    }
    
    @Override
    public Long getKey(Genre entity) {
        if(entity != null) {
            return entity.getId();
        } else {
            return null;
        }
    }

    @Override
    public boolean hasKey(Genre entity) {
        return entity.getId() != null;
    }

    @Override
    protected final boolean isEntityUpdateable() {
        return true;
    }
    
    /** Internal query to resolve the "genres" to-many relationship of Movie. */
    public List<Genre> _queryMovie_Genres(Long movieId) {
        synchronized (this) {
            if (movie_GenresQuery == null) {
                QueryBuilder<Genre> queryBuilder = queryBuilder();
                queryBuilder.join(Link.class, LinkDao.Properties.GenreId)
                        .where(LinkDao.Properties.MovieId.eq(movieId));
                movie_GenresQuery = queryBuilder.build();
            }
        }
        Query<Genre> query = movie_GenresQuery.forCurrentThread();
        query.setParameter(0, movieId);
        return query.list();
    }

}
