package com.damsky.danny.bambatime.util;

import android.os.Build;

/**
 * This class contains constant information which is used throughout the application.
 *
 * @author Danny Damsky
 */

public final class Constants {

    /**
     * Genre and Movie field keywords (Also keywords used by the TMDB API)
     */
    public static final String SEARCH_RESULTS = "results";
    public static final String GENRE_ID = "id";
    public static final String GENRE_NAME = "name";
    public static final String GENRE_ARRAY = "genres";
    public static final String MOVIE_ID = "id";
    public static final String MOVIE_ADULT = "adult";
    public static final String MOVIE_TITLE = "title";
    public static final String MOVIE_TAGLINE = "tagline";
    public static final String MOVIE_OVERVIEW = "overview";
    public static final String MOVIE_RELEASE_DATE = "release_date";
    public static final String MOVIE_ORIGINAL_LANGUAGE = "original_language";
    public static final String MOVIE_ORIGINAL_TITLE = "original_title";
    public static final String MOVIE_VOTE_COUNT = "vote_count";
    public static final String MOVIE_VOTE_AVERAGE = "vote_average";
    public static final String MOVIE_POPULARITY = "popularity";
    public static final String MOVIE_BUDGET = "budget";
    public static final String MOVIE_REVENUE = "revenue";
    public static final String MOVIE_RUNTIME = "runtime";
    public static final String MOVIE_POSTER_PATH = "poster_path";
    public static final String MOVIE_BACKDROP_PATH = "backdrop_path";
    public static final String MOVIE_HOMEPAGE = "homepage";
    public static final String MOVIE_IMDB_ID = "imdb_id";
    public static final String MOVIE_STATUS = "status";

    /**
     * Extra Fields (Not used by APIs)
     */
    public static final String MOVIE_RELEASE_DATE_YEAR = "release_date_year";
    public static final String MOVIE_RELEASE_DATE_MONTH = "release_date_month";
    public static final String MOVIE_RELEASE_DATE_DAY = "release_date_day";
    public static final String MOVIE_RUNTIME_HOUR = "runtime_hour";
    public static final String MOVIE_RUNTIME_MINUTE = "runtime_minute";
    public static final String MOVIE_POSTER_PATH_USER = "poster_path_user";
    public static final String MOVIE_BACKDROP_PATH_USER = "backdrop_path_user";

    /**
     * Intent Extras
     */
    public static final String EXTRA_ADULT_SETTING_CHANGED = "adult_setting_changed";
    public static final String EXTRA_RESTART_MOVIE_LOAD_TASK = "restart_movie_load_task";
    public static final String EXTRA_RESTART_SOURCE_CODE_LOADER_TASK = "restart_source_code_loader_task";
    public static final String EXTRA_NIGHT_MODE_CHANGED = "night_mode_changed";
    public static final String EXTRA_MOVIE_POSITION_IN_LIST = "movie_position_in_list";
    public static final String EXTRA_MOVIE_ID = "movie_id";
    public static final String EXTRA_SEARCH_INPUT = "extra_search_input";
    public static final String EXTRA_BUNDLE = "extra_bundle";

    /**
     * Runtime Information
     */
    public static final boolean IS_MARSHMALLOW_OR_ABOVE = Build.VERSION.SDK_INT >= Build.VERSION_CODES.M;
    public static final int BACK_BUTTON_DOUBLE_TAP_INTERVAL = 1000; // Milliseconds (1000 ms = 1 sec)


    /**
     * Request Codes
     */
    public static final int REQUEST_CODE_PREFERENCES = 101;
    public static final int REQUEST_CODE_MOVIE_ACTIVITY = 102;
    public static final int REQUEST_CODE_GALLERY = 103;
    public static final int REQUEST_CODE_CAMERA = 104;
    public static final int REQUEST_CODE_PERMISSIONS_WRITE_EXTERNAL_STORAGE = 105;

    /**
     * Result Codes
     */
    public static final int RESULT_CODE_POSTER = 101;
    public static final int RESULT_CODE_BACKDROP = 102;
    public static final int RESULT_CODE_DIALOG_OK = 103;

    /**
     * Constants used to pass arguments in DialogFragments
     */
    public static final String DIALOG_TITLE = "dialog_title";
    public static final String DIALOG_MESSAGE = "dialog_message";
    public static final String DIALOG_POSITIVE_BUTTON_TEXT = "dialog_positive_button_text";

    public static final String URL_EMAIL = "mailto:dannydamskypublic@gmail.com";
    public static final String URL_STORE = "market://details?id=com.damsky.danny.bambatime";
    public static final String URL_STORE_BACKUP = "https://play.google.com/store/apps/details?id=com.damsky.danny.bambatime";
    public static final String URL_SOURCE_CODE = "https://bitbucket.org/dannydamsky/bambatime";

    private Constants() {
    }
}
