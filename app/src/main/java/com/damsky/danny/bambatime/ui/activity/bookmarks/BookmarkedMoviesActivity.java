package com.damsky.danny.bambatime.ui.activity.bookmarks;

import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.app.AppCompatDelegate;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.Surface;
import android.view.View;
import android.view.WindowManager;
import android.widget.TextView;

import com.damsky.danny.bambatime.App;
import com.damsky.danny.bambatime.R;
import com.damsky.danny.bambatime.data.localdb.model.Movie;
import com.damsky.danny.bambatime.ui.activity.about.AboutPageActivity;
import com.damsky.danny.bambatime.ui.activity.main.MainActivity;
import com.damsky.danny.bambatime.ui.activity.main.adapter.MainRecycleAdapter;
import com.damsky.danny.bambatime.ui.activity.movie.MovieActivity;
import com.damsky.danny.bambatime.ui.activity.mymovies.MyMoviesActivity;
import com.damsky.danny.bambatime.ui.activity.prefs.PreferencesActivity;
import com.damsky.danny.bambatime.ui.activity.search.SearchActivity;
import com.damsky.danny.bambatime.ui.dialog.alertdialog.SimpleAlertDialog;
import com.damsky.danny.bambatime.util.Constants;

import java.util.Collections;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * This activity uses the same layout as the MainActivity.
 * The purpose of this activity is to show the user's bookmarked movies in the DB.
 *
 * @author Danny Damsky
 * @see MainActivity
 * @see MainRecycleAdapter
 * @see SimpleAlertDialog
 */

public final class BookmarkedMoviesActivity extends AppCompatActivity implements
        NavigationView.OnNavigationItemSelectedListener, MainRecycleAdapter.OnCardClickListener,
        SimpleAlertDialog.OnAlertDialogDismissListener {

    private List<Movie> movies;
    private App appReference;

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.mainRecyclerView)
    RecyclerView mainRecyclerView;
    @BindView(R.id.drawerLayout)
    DrawerLayout drawerLayout;
    @BindView(R.id.navigationDrawer)
    NavigationView navigationDrawer;
    @BindView(R.id.emptyError)
    TextView emptyError;
    @BindView(R.id.container)
    CoordinatorLayout container;

    private Movie movieToRemoveFromBookmarks;

    private boolean doubleTapToExit = false;

    private boolean isLandscape() {
        int rotationDegrees = ((WindowManager) getSystemService(WINDOW_SERVICE))
                .getDefaultDisplay()
                .getRotation();

        return rotationDegrees == Surface.ROTATION_90 || rotationDegrees == Surface.ROTATION_270;
    }

    private boolean isLargeScreen() {
        int screenSize = getResources().getConfiguration().screenLayout &
                Configuration.SCREENLAYOUT_SIZE_MASK;

        return screenSize == Configuration.SCREENLAYOUT_SIZE_LARGE ||
                screenSize == Configuration.SCREENLAYOUT_SIZE_XLARGE;
    }

    private void updateRecyclerViewAdapter() {
        mainRecyclerView.getAdapter().notifyDataSetChanged();

        if (movies.size() == 0)
            emptyError.setVisibility(View.VISIBLE);
        else
            emptyError.setVisibility(View.GONE);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        appReference = (App) getApplication();
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        setSupportActionBar(toolbar);

        ActionBarDrawerToggle mToggle = new ActionBarDrawerToggle(this, drawerLayout, toolbar,
                R.string.open_nav_drawer, R.string.close_nav_drawer);

        drawerLayout.addDrawerListener(mToggle);
        mToggle.syncState();

        navigationDrawer.setNavigationItemSelectedListener(this);
        navigationDrawer.getMenu().findItem(R.id.action_bookmarked).setChecked(true);

        boolean shouldEnableBigLayout = isLandscape() || isLargeScreen();
        if (shouldEnableBigLayout)
            mainRecyclerView.setLayoutManager(new GridLayoutManager(this, 2));
        else
            mainRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        mainRecyclerView.setItemAnimator(new DefaultItemAnimator());

        movies = appReference.getDbHelper().getBookmarkedMovies();
        mainRecyclerView.setAdapter(new MainRecycleAdapter(this, movies, shouldEnableBigLayout));

        if (movies.size() == 0)
            emptyError.setVisibility(View.VISIBLE);

        emptyError.setText(R.string.error_empty_bookmarks);
    }

    @Override
    public void onBackPressed() {
        if (drawerLayout.isDrawerOpen(navigationDrawer))
            drawerLayout.closeDrawer(navigationDrawer);

        else if (doubleTapToExit)
            super.onBackPressed();

        else {
            final Snackbar snackbar = Snackbar
                    .make(container, R.string.double_tap_message, Snackbar.LENGTH_INDEFINITE);
            snackbar.show();
            doubleTapToExit = true;
            new Handler().postDelayed(() -> {
                doubleTapToExit = false;
                snackbar.dismiss();
            }, Constants.BACK_BUTTON_DOUBLE_TAP_INTERVAL);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        MenuItem sortBy = menu.findItem(R.id.action_sort_by);
        sortBy.setVisible(true);
        onOptionsItemSelected(menu.findItem(R.id.popularity_desc));
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.popularity_desc:
                Collections.sort(movies, (o1, o2) ->
                        Double.compare(o2.getPopularity(), o1.getPopularity()));
                break;
            case R.id.popularity_asc:
                Collections.sort(movies, (o1, o2) ->
                        Double.compare(o1.getPopularity(), o2.getPopularity()));
                break;
            case R.id.rating_desc:
                Collections.sort(movies, (o1, o2) ->
                        Double.compare(o2.getVote_average(), o1.getVote_average()));
                break;
            case R.id.rating_asc:
                Collections.sort(movies, (o1, o2) ->
                        Double.compare(o1.getVote_average(), o2.getVote_average()));
                break;
            case R.id.release_date_desc:
                Collections.sort(movies, (o1, o2) ->
                        o2.getRelease_date().compareTo(o1.getRelease_date()));
                break;
            case R.id.release_date_asc:
                Collections.sort(movies, (o1, o2) ->
                        o1.getRelease_date().compareTo(o2.getRelease_date()));
                break;
            case R.id.title_asc:
                Collections.sort(movies, (o1, o2) ->
                        o1.getTitle().compareTo(o2.getTitle()));
                break;
            case R.id.title_desc:
                Collections.sort(movies, (o1, o2) ->
                        o2.getTitle().compareTo(o1.getTitle()));
                break;
        }

        item.setChecked(true);
        updateRecyclerViewAdapter();
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_settings:
                Intent startPrefs = new Intent(this, PreferencesActivity.class);
                startPrefs.putExtra(Constants.EXTRA_NIGHT_MODE_CHANGED, false);
                startActivityForResult(startPrefs, Constants.REQUEST_CODE_PREFERENCES);
                break;

            case R.id.action_discover:
                Intent startMain = new Intent(this, MainActivity.class);
                startActivity(startMain);
                finish();
                break;

            case R.id.action_my_movies:
                Intent startMyMovies = new Intent(this, MyMoviesActivity.class);
                startActivity(startMyMovies);
                finish();
                break;

            case R.id.action_search:
                Intent startSearch = new Intent(this, SearchActivity.class);
                startActivity(startSearch);
                finish();
                break;

            case R.id.action_about:
                Intent startAbout = new Intent(this, AboutPageActivity.class);
                startActivity(startAbout);
                break;
        }

        drawerLayout.closeDrawer(navigationDrawer);
        return true;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case Constants.REQUEST_CODE_PREFERENCES:
                if (data != null && data.getBooleanExtra(Constants.EXTRA_NIGHT_MODE_CHANGED, false)) {
                    AppCompatDelegate.setDefaultNightMode(appReference.getPreferencesHelper().getTheme());
                    this.recreate();
                }
                break;

            case Constants.REQUEST_CODE_MOVIE_ACTIVITY:
                if (movies != null) {
                    int positionInList = data.getIntExtra(Constants.EXTRA_MOVIE_POSITION_IN_LIST, -1);
                    Long movieId = movies.get(positionInList).getId();
                    Movie movie = appReference.getDbHelper().getMovieById(movieId);

                    if (movie.getBookmarked())
                        movies.set(positionInList, movie);
                    else
                        movies.remove(positionInList);

                    updateRecyclerViewAdapter();
                }
                break;
        }
    }

    @Override
    public void onCardClicked(int position) {
        Movie movieClicked = movies.get(position);
        Intent intent = new Intent(this, MovieActivity.class);
        intent.putExtra(Constants.EXTRA_MOVIE_ID, movieClicked.getId());
        intent.putExtra(Constants.EXTRA_MOVIE_POSITION_IN_LIST, position);
        startActivityForResult(intent, Constants.REQUEST_CODE_MOVIE_ACTIVITY);

    }

    @Override
    public void onCardLongClicked(int position) {
        movieToRemoveFromBookmarks = movies.get(position);
        SimpleAlertDialog.show(getSupportFragmentManager(), R.string.confirm_action, R.string.confirm_action_remove_from_bookmarks);
    }

    @Override
    public void onAlertDialogOkPressed() {
        movieToRemoveFromBookmarks.setBookmarked(false);
        appReference.getDbHelper().updateMovie(movieToRemoveFromBookmarks);
        movies.remove(movieToRemoveFromBookmarks);
        movieToRemoveFromBookmarks = null;
        updateRecyclerViewAdapter();
    }
}
