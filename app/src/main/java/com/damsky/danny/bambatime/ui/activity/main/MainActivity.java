package com.damsky.danny.bambatime.ui.activity.main;

import android.content.Intent;
import android.content.res.Configuration;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.v4.util.Pair;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.app.AppCompatDelegate;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.Surface;
import android.view.View;
import android.view.WindowManager;
import android.widget.TextView;

import com.damsky.danny.bambatime.App;
import com.damsky.danny.bambatime.R;
import com.damsky.danny.bambatime.data.localdb.model.Movie;
import com.damsky.danny.bambatime.ui.activity.about.AboutPageActivity;
import com.damsky.danny.bambatime.ui.activity.bookmarks.BookmarkedMoviesActivity;
import com.damsky.danny.bambatime.ui.activity.main.adapter.MainRecycleAdapter;
import com.damsky.danny.bambatime.ui.activity.movie.MovieActivity;
import com.damsky.danny.bambatime.ui.activity.mymovies.MyMoviesActivity;
import com.damsky.danny.bambatime.ui.activity.prefs.PreferencesActivity;
import com.damsky.danny.bambatime.ui.activity.search.SearchActivity;
import com.damsky.danny.bambatime.util.Constants;
import com.damsky.danny.bambatime.util.task.MovieQueryTask;
import com.damsky.danny.bambatime.util.task.NetworkTask;
import com.damsky.danny.bambatime.util.task.SearchTask;

import java.util.Collections;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * This activity starts immediately after SplashActivity finishes loading JSONs for it.
 * In this activity the user is presented with TMDBs Discover page filtered to show movies that were
 * released in the current year.
 *
 * @author Danny Damsky
 * @see MainRecycleAdapter
 * @see NetworkTask
 * @see SearchTask
 * @see MovieQueryTask
 */

public final class MainActivity extends AppCompatActivity implements
        NavigationView.OnNavigationItemSelectedListener, MainRecycleAdapter.OnCardClickListener {

    private List<Movie> movies;
    private App appReference;

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.mainRecyclerView)
    RecyclerView mainRecyclerView;
    @BindView(R.id.drawerLayout)
    DrawerLayout drawerLayout;
    @BindView(R.id.container)
    CoordinatorLayout container;
    @BindView(R.id.navigationDrawer)
    NavigationView navigationDrawer;
    @BindView(R.id.emptyError)
    TextView emptyError;

    MenuItem sortBy;

    private SearchTask movieLoadTask;
    private NetworkTask sourceCodeLoaderTask;
    private MovieQueryTask movieQueryTask;

    private boolean doubleTapToExit = false;

    /* This method parses the JSON of movie results that it gets from cache
    stored in SharedPreferences and sets the adapter for the recycler view,
    so when this method finishes executing the user will see a list of movies. */

    private void asyncLoadMovies(boolean shouldEnableBigLayout) {
        movieLoadTask = new SearchTask(simpleMovies -> {
            movies = simpleMovies;
            mainRecyclerView.setAdapter(new MainRecycleAdapter(this, movies, shouldEnableBigLayout));

            if (movies.size() == 0)
                emptyError.setVisibility(View.VISIBLE);
            else
                emptyError.setVisibility(View.GONE);

            getIntent().putExtra(Constants.EXTRA_RESTART_MOVIE_LOAD_TASK, false);

            asyncReadyMovies(0);
        });

        movieLoadTask.execute(appReference.getPreferencesHelper().getDiscoverCache());
    }

    /* This method gets called from asyncLoadMovies() after it's done parsing the JSON of movie results.
       This method is recursive, in the sense that it calls itself after a certain action is done.
       This method is in charge of completing the movies (I.E. "readying" them), it queries each
       movie from the available list using the TMDB API (or gets the movie if it's already available
       in the DB) and adds all the missing fields to the appropriate object after it's done parsing
       the JSON of the query. */

    private void asyncReadyMovies(int movieIndex) {
        if (movieIndex < movies.size()) {
            Movie movie = movies.get(movieIndex);
            Movie getMovie = appReference.getDbHelper().getMovieByTmdbId(movie.getTmdbId());

            if (getMovie != null && (getMovie.getTitle().equals(movie.getTitle()) || getMovie.getModifiable())) {
                movies.set(movieIndex, getMovie);
                asyncReadyMovies(movieIndex + 1);
            } else {
                sourceCodeLoaderTask = new NetworkTask(new NetworkTask.NetworkTaskCallback() {
                    @SuppressWarnings("unchecked")
                    @Override
                    public void onDownloadSucceeded(String webSource) {
                        Pair<String, Movie> passToTask = new Pair<>(webSource, movie);

                        movieQueryTask = new MovieQueryTask(genres -> {
                            appReference.getDbHelper().insertMovieWithGenres(movie, genres);
                            asyncReadyMovies(movieIndex + 1);
                        });

                        movieQueryTask.execute(passToTask);
                    }

                    @Override
                    public void onDownloadFailed() {
                        asyncReadyMovies(movieIndex + 1);
                    }
                });

                sourceCodeLoaderTask.execute(getString(R.string.tmdb_movies_query, movie.getTmdbId()));
            }
        } else {
            getIntent().putExtra(Constants.EXTRA_RESTART_SOURCE_CODE_LOADER_TASK, false);
            if (sortBy != null)
                sortBy.setVisible(true);

            mainRecyclerView.getAdapter().notifyDataSetChanged();
        }
    }

    private boolean isLandscape() {
        int rotationDegrees = ((WindowManager) getSystemService(WINDOW_SERVICE))
                .getDefaultDisplay()
                .getRotation();

        return rotationDegrees == Surface.ROTATION_90 || rotationDegrees == Surface.ROTATION_270;
    }

    private boolean isLargeScreen() {
        int screenSize = getResources().getConfiguration().screenLayout &
                Configuration.SCREENLAYOUT_SIZE_MASK;

        return screenSize == Configuration.SCREENLAYOUT_SIZE_LARGE ||
                screenSize == Configuration.SCREENLAYOUT_SIZE_XLARGE;
    }

    private boolean taskRunning(AsyncTask task) {
        return task != null && task.getStatus() == AsyncTask.Status.RUNNING;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        appReference = (App) getApplication();
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        setSupportActionBar(toolbar);

        ActionBarDrawerToggle mToggle = new ActionBarDrawerToggle(this, drawerLayout, toolbar,
                R.string.open_nav_drawer, R.string.close_nav_drawer);

        drawerLayout.addDrawerListener(mToggle);
        mToggle.syncState();

        navigationDrawer.setNavigationItemSelectedListener(this);

        boolean shouldEnableBigLayout = isLandscape() || isLargeScreen();
        if (shouldEnableBigLayout)
            mainRecyclerView.setLayoutManager(new GridLayoutManager(this, 2));
        else
            mainRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        mainRecyclerView.setItemAnimator(new DefaultItemAnimator());

        asyncLoadMovies(shouldEnableBigLayout);

        emptyError.setText(R.string.error_empty_main);
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (!(taskRunning(movieLoadTask) ||
                taskRunning(movieQueryTask) ||
                taskRunning(sourceCodeLoaderTask))) {

            if (getIntent().getBooleanExtra(Constants.EXTRA_RESTART_MOVIE_LOAD_TASK, true))
                asyncLoadMovies(isLandscape() || isLargeScreen());
            else if (getIntent().getBooleanExtra(Constants.EXTRA_RESTART_SOURCE_CODE_LOADER_TASK, true))
                asyncReadyMovies(0);
        }
    }

    @Override
    protected void onPause() {
        if (taskRunning(movieLoadTask))
            movieLoadTask.cancel(true);

        if (taskRunning(sourceCodeLoaderTask))
            sourceCodeLoaderTask.cancel(true);

        if (taskRunning(movieQueryTask))
            movieQueryTask.cancel(true);

        super.onPause();
    }

    @Override
    public void onBackPressed() {
        if (drawerLayout.isDrawerOpen(navigationDrawer))
            drawerLayout.closeDrawer(navigationDrawer);

        else if (doubleTapToExit)
            super.onBackPressed();

        else {
            final Snackbar snackbar = Snackbar
                    .make(container, R.string.double_tap_message, Snackbar.LENGTH_INDEFINITE);
            snackbar.show();
            doubleTapToExit = true;
            new Handler().postDelayed(() -> {
                doubleTapToExit = false;
                snackbar.dismiss();
            }, Constants.BACK_BUTTON_DOUBLE_TAP_INTERVAL);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        sortBy = menu.findItem(R.id.action_sort_by);

        if (movieLoadTask.getStatus() == AsyncTask.Status.FINISHED)
            sortBy.setVisible(true);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.popularity_desc:
                Collections.sort(movies, (o1, o2) ->
                        Double.compare(o2.getPopularity(), o1.getPopularity()));
                break;
            case R.id.popularity_asc:
                Collections.sort(movies, (o1, o2) ->
                        Double.compare(o1.getPopularity(), o2.getPopularity()));
                break;
            case R.id.rating_desc:
                Collections.sort(movies, (o1, o2) ->
                        Double.compare(o2.getVote_average(), o1.getVote_average()));
                break;
            case R.id.rating_asc:
                Collections.sort(movies, (o1, o2) ->
                        Double.compare(o1.getVote_average(), o2.getVote_average()));
                break;
            case R.id.release_date_desc:
                Collections.sort(movies, (o1, o2) ->
                        o2.getRelease_date().compareTo(o1.getRelease_date()));
                break;
            case R.id.release_date_asc:
                Collections.sort(movies, (o1, o2) ->
                        o1.getRelease_date().compareTo(o2.getRelease_date()));
                break;
            case R.id.title_asc:
                Collections.sort(movies, (o1, o2) ->
                        o1.getTitle().compareTo(o2.getTitle()));
                break;
            case R.id.title_desc:
                Collections.sort(movies, (o1, o2) ->
                        o2.getTitle().compareTo(o1.getTitle()));
                break;
        }

        item.setChecked(true);
        mainRecyclerView.getAdapter().notifyDataSetChanged();
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_settings:
                Intent startPrefs = new Intent(this, PreferencesActivity.class);
                startPrefs.putExtra(Constants.EXTRA_NIGHT_MODE_CHANGED, false);
                startActivityForResult(startPrefs, Constants.REQUEST_CODE_PREFERENCES);
                break;

            case R.id.action_my_movies:
                Intent startMyMovies = new Intent(this, MyMoviesActivity.class);
                startActivity(startMyMovies);
                finish();
                break;

            case R.id.action_bookmarked:
                Intent startBookmarkedMovies = new Intent(this, BookmarkedMoviesActivity.class);
                startActivity(startBookmarkedMovies);
                finish();
                break;

            case R.id.action_search:
                Intent startSearch = new Intent(this, SearchActivity.class);
                startActivity(startSearch);
                finish();
                break;

            case R.id.action_about:
                Intent startAbout = new Intent(this, AboutPageActivity.class);
                startActivity(startAbout);
                break;
        }

        drawerLayout.closeDrawer(navigationDrawer);
        return true;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case Constants.REQUEST_CODE_PREFERENCES:
                if (data != null) {
                    if (data.getBooleanExtra(Constants.EXTRA_NIGHT_MODE_CHANGED, false)) {
                        AppCompatDelegate.setDefaultNightMode(appReference.getPreferencesHelper().getTheme());
                        this.recreate();
                    }
                }
                break;

            case Constants.REQUEST_CODE_MOVIE_ACTIVITY:
                if (movies != null) {
                    int positionInList = data.getIntExtra(Constants.EXTRA_MOVIE_POSITION_IN_LIST, -1);
                    Long movieId = movies.get(positionInList).getId();
                    movies.set(positionInList, appReference.getDbHelper().getMovieById(movieId));
                    mainRecyclerView.getAdapter().notifyDataSetChanged();
                }
                break;
        }
    }

    @Override
    public void onCardClicked(int position) {
        Movie movieClicked = movies.get(position);
        if (movieClicked.isReady()) {
            Intent intent = new Intent(this, MovieActivity.class);
            intent.putExtra(Constants.EXTRA_MOVIE_ID, movieClicked.getId());
            intent.putExtra(Constants.EXTRA_MOVIE_POSITION_IN_LIST, position);
            startActivityForResult(intent, Constants.REQUEST_CODE_MOVIE_ACTIVITY);
        } else
            Snackbar.make(container, R.string.movie_loading_message, Snackbar.LENGTH_LONG).show();
    }
}
