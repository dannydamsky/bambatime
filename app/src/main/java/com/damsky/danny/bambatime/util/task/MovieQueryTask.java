package com.damsky.danny.bambatime.util.task;

import android.os.AsyncTask;
import android.support.annotation.NonNull;
import android.support.v4.util.Pair;
import android.util.Log;

import com.damsky.danny.bambatime.data.localdb.model.Genre;
import com.damsky.danny.bambatime.data.localdb.model.Movie;
import com.damsky.danny.bambatime.util.Constants;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * This task is used to set the parameters of the movie that are only available from
 * the movie's JSON and not from Search/Discover results.
 *
 * @author Danny Damsky
 */

public final class MovieQueryTask extends AsyncTask<Pair<String, Movie>, Void, List<Genre>> {

    private MovieQueryTaskCallback callback;
    private boolean errorHappened = false;

    public MovieQueryTask(@NonNull MovieQueryTaskCallback callback) {
        this.callback = callback;
    }

    /**
     * @param pairs There should only be one object, the first value should be a String containing
     *              a JSON of the movie. The second value should be an object of said movie.
     * @return A list of genres that belong to the movie, or null if the JSON parsing failed.
     */
    @Override
    protected List<Genre> doInBackground(Pair<String, Movie>[] pairs) {
        List<Genre> thisGenres = null;
        try {
            Pair<String, Movie> pair = pairs[0];
            JSONObject thisResult = new JSONObject(pair.first);

            String original_language = thisResult.getString(Constants.MOVIE_ORIGINAL_LANGUAGE);
            String original_title = thisResult.getString(Constants.MOVIE_ORIGINAL_TITLE);
            int vote_count = thisResult.getInt(Constants.MOVIE_VOTE_COUNT);
            String tagline = thisResult.getString(Constants.MOVIE_TAGLINE);
            int budget = thisResult.getInt(Constants.MOVIE_BUDGET);
            int revenue = thisResult.getInt(Constants.MOVIE_REVENUE);
            int runtime;
            try {
                runtime = thisResult.getInt(Constants.MOVIE_RUNTIME);
            } catch (Exception e) {
                runtime = -1;
            }

            String homepage = thisResult.getString(Constants.MOVIE_HOMEPAGE);
            String imdb_id = thisResult.getString(Constants.MOVIE_IMDB_ID);
            String status = thisResult.getString(Constants.MOVIE_STATUS);

            if (pair.second != null)
                pair.second.complete(original_language, original_title, vote_count,
                        tagline, budget, revenue, runtime, homepage, imdb_id, status);

            JSONArray jsonGenres = thisResult.getJSONArray(Constants.GENRE_ARRAY);
            thisGenres = new ArrayList<>(jsonGenres.length());

            for (int j = 0; j < jsonGenres.length(); j++) {
                JSONObject genre = jsonGenres.getJSONObject(j);
                Long genreId = genre.getLong(Constants.GENRE_ID);
                String genreName = genre.getString(Constants.GENRE_NAME);
                thisGenres.add(new Genre(genreId, genreName));
            }

        } catch (Exception e) {
            // This should never happen. If it does, the JSON was incorrect.
            Log.e("ErrorInMovieQueryTask", e.getMessage());
            errorHappened = true;
        }

        return thisGenres;
    }

    @Override
    protected void onPostExecute(List<Genre> genres) {
        super.onPostExecute(genres);
        if (!errorHappened)
            callback.onMovieQuerySucceeded(genres);

        callback = null;
    }

    public interface MovieQueryTaskCallback {
        void onMovieQuerySucceeded(List<Genre> genres);
    }
}
