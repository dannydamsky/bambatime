package com.damsky.danny.bambatime.ui.activity.movie.adapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.damsky.danny.bambatime.R;
import com.damsky.danny.bambatime.data.localdb.model.Genre;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * An adapter for a RecyclerView of Genres.
 *
 * @author Danny Damsky
 */

public final class GenreCardAdapter extends RecyclerView.Adapter<GenreCardAdapter.ViewHolder> {

    private final List<Genre> genres;
    private final OnCardClickListener listener;

    public GenreCardAdapter(@NonNull OnCardClickListener listener, List<Genre> genres) {
        this.listener = listener;
        this.genres = genres;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.recyclerview_genre_item, parent, false);

        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.card.setOnClickListener(v -> listener.onCardClicked(position));
        holder.genreTextView.setText(genres.get(position).getName());
    }

    @Override
    public int getItemCount() {
        return genres.size();
    }

    public interface OnCardClickListener {
        void onCardClicked(int position);
    }

    static class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.card)
        CardView card;
        @BindView(R.id.genreTextView)
        TextView genreTextView;

        ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
