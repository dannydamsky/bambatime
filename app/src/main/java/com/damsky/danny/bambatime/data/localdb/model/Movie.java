package com.damsky.danny.bambatime.data.localdb.model;

import org.greenrobot.greendao.DaoException;
import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Generated;
import org.greenrobot.greendao.annotation.Id;
import org.greenrobot.greendao.annotation.JoinEntity;
import org.greenrobot.greendao.annotation.Keep;
import org.greenrobot.greendao.annotation.ToMany;
import org.greenrobot.greendao.annotation.Transient;
import org.greenrobot.greendao.annotation.Unique;

import java.util.List;

@Entity(nameInDb = "MOVIES")
public class Movie {

    @Id(autoincrement = true)
    private Long id;

    @Unique
    private Long tmdbId;

    private boolean adult;

    private String title;

    private String tagline;

    private String overview;

    private String release_date;

    private String original_language;

    private String original_title;

    private int vote_count;

    private double vote_average;

    private double popularity;

    private int budget;

    private int revenue;

    private int runtime;

    private String status;

    private String poster_path;

    private String backdrop_path;

    private String homepage;

    private String imdb_id;

    private boolean modifiable;

    private boolean bookmarked;

    private boolean is_seen;

    private String poster_path_user;

    private String backdrop_path_user;

    @Transient
    private boolean isReady = true;

    @ToMany
    @JoinEntity(
            entity = Link.class,
            sourceProperty = "movieId",
            targetProperty = "genreId"
    )
    private List<Genre> genres;

    /**
     * Used to resolve relations
     */
    @Generated(hash = 2040040024)
    private transient DaoSession daoSession;

    /**
     * Used for active entity operations.
     */
    @Generated(hash = 1042217376)
    private transient MovieDao myDao;

    @Generated(hash = 1030170904)
    public Movie(Long id, Long tmdbId, boolean adult, String title, String tagline, String overview,
                 String release_date, String original_language, String original_title, int vote_count,
                 double vote_average, double popularity, int budget, int revenue, int runtime, String status,
                 String poster_path, String backdrop_path, String homepage, String imdb_id,
                 boolean modifiable, boolean bookmarked, boolean is_seen, String poster_path_user,
                 String backdrop_path_user) {
        this.id = id;
        this.tmdbId = tmdbId;
        this.adult = adult;
        this.title = title;
        this.tagline = tagline;
        this.overview = overview;
        this.release_date = release_date;
        this.original_language = original_language;
        this.original_title = original_title;
        this.vote_count = vote_count;
        this.vote_average = vote_average;
        this.popularity = popularity;
        this.budget = budget;
        this.revenue = revenue;
        this.runtime = runtime;
        this.status = status;
        this.poster_path = poster_path;
        this.backdrop_path = backdrop_path;
        this.homepage = homepage;
        this.imdb_id = imdb_id;
        this.modifiable = modifiable;
        this.bookmarked = bookmarked;
        this.is_seen = is_seen;
        this.poster_path_user = poster_path_user;
        this.backdrop_path_user = backdrop_path_user;
    }

    @Generated(hash = 1263461133)
    public Movie() {
    }

    @Keep
    public Movie(Long tmdbId, boolean adult, String title, String overview, String release_date,
                 double vote_average, double popularity, String poster_path, String backdrop_path) {
        this.id = null;
        this.tmdbId = tmdbId;
        this.adult = adult;
        this.title = title;
        this.tagline = null;
        this.overview = overview;
        this.release_date = release_date;
        this.original_language = null;
        this.original_title = null;
        this.vote_count = -1;
        this.vote_average = vote_average;
        this.popularity = popularity;
        this.budget = -1;
        this.revenue = -1;
        this.status = null;
        this.poster_path = poster_path;
        this.backdrop_path = backdrop_path;
        this.homepage = null;
        this.imdb_id = null;
        this.modifiable = false;
        this.bookmarked = false;
        this.is_seen = false;
        this.poster_path_user = null;
        this.backdrop_path_user = null;
        this.isReady = false;
    }

    @Keep
    public void complete(String original_language,
                         String original_title, int vote_count, String tagline, int budget,
                         int revenue, int runtime, String homepage, String imdb_id, String status) {
        this.original_language = original_language;
        this.original_title = original_title;
        this.vote_count = vote_count;
        this.tagline = tagline;
        this.budget = budget;
        this.revenue = revenue;
        this.runtime = runtime;
        this.homepage = homepage;
        this.imdb_id = imdb_id;
        this.status = status;
        this.isReady = true;
    }

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getTmdbId() {
        return this.tmdbId;
    }

    public void setTmdbId(Long tmdbId) {
        this.tmdbId = tmdbId;
    }

    public boolean getAdult() {
        return this.adult;
    }

    public void setAdult(boolean adult) {
        this.adult = adult;
    }

    public String getTitle() {
        return this.title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getTagline() {
        return this.tagline;
    }

    public void setTagline(String tagline) {
        this.tagline = tagline;
    }

    public String getOverview() {
        return this.overview;
    }

    public void setOverview(String overview) {
        this.overview = overview;
    }

    public String getRelease_date() {
        return this.release_date;
    }

    public void setRelease_date(String release_date) {
        this.release_date = release_date;
    }

    public String getOriginal_language() {
        return this.original_language;
    }

    public void setOriginal_language(String original_language) {
        this.original_language = original_language;
    }

    public String getOriginal_title() {
        return this.original_title;
    }

    public void setOriginal_title(String original_title) {
        this.original_title = original_title;
    }

    public int getVote_count() {
        return this.vote_count;
    }

    public void setVote_count(int vote_count) {
        this.vote_count = vote_count;
    }

    public double getVote_average() {
        return this.vote_average;
    }

    public void setVote_average(double vote_average) {
        this.vote_average = vote_average;
    }

    public double getPopularity() {
        return this.popularity;
    }

    public void setPopularity(double popularity) {
        this.popularity = popularity;
    }

    public int getBudget() {
        return this.budget;
    }

    public void setBudget(int budget) {
        this.budget = budget;
    }

    public int getRevenue() {
        return this.revenue;
    }

    public void setRevenue(int revenue) {
        this.revenue = revenue;
    }

    public int getRuntime() {
        return this.runtime;
    }

    public void setRuntime(int runtime) {
        this.runtime = runtime;
    }

    public String getStatus() {
        return this.status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getPoster_path() {
        return this.poster_path;
    }

    public void setPoster_path(String poster_path) {
        this.poster_path = poster_path;
    }

    public String getBackdrop_path() {
        return this.backdrop_path;
    }

    public void setBackdrop_path(String backdrop_path) {
        this.backdrop_path = backdrop_path;
    }

    public String getHomepage() {
        return this.homepage;
    }

    public void setHomepage(String homepage) {
        this.homepage = homepage;
    }

    public String getImdb_id() {
        return this.imdb_id;
    }

    public void setImdb_id(String imdb_id) {
        this.imdb_id = imdb_id;
    }

    public boolean getModifiable() {
        return this.modifiable;
    }

    public void setModifiable(boolean modifiable) {
        this.modifiable = modifiable;
    }

    public boolean getBookmarked() {
        return this.bookmarked;
    }

    public void setBookmarked(boolean bookmarked) {
        this.bookmarked = bookmarked;
    }

    public boolean getIs_seen() {
        return this.is_seen;
    }

    public void setIs_seen(boolean is_seen) {
        this.is_seen = is_seen;
    }

    public boolean isReady() {
        return isReady;
    }

    /**
     * To-many relationship, resolved on first access (and after reset).
     * Changes to to-many relations are not persisted, make changes to the target entity.
     */
    @Generated(hash = 929177057)
    public List<Genre> getGenres() {
        if (genres == null) {
            final DaoSession daoSession = this.daoSession;
            if (daoSession == null) {
                throw new DaoException("Entity is detached from DAO context");
            }
            GenreDao targetDao = daoSession.getGenreDao();
            List<Genre> genresNew = targetDao._queryMovie_Genres(id);
            synchronized (this) {
                if (genres == null) {
                    genres = genresNew;
                }
            }
        }
        return genres;
    }

    /**
     * Resets a to-many relationship, making the next get call to query for a fresh result.
     */
    @Generated(hash = 1988821389)
    public synchronized void resetGenres() {
        genres = null;
    }

    /**
     * Convenient call for {@link org.greenrobot.greendao.AbstractDao#delete(Object)}.
     * Entity must attached to an entity context.
     */
    @Generated(hash = 128553479)
    public void delete() {
        if (myDao == null) {
            throw new DaoException("Entity is detached from DAO context");
        }
        myDao.delete(this);
    }

    /**
     * Convenient call for {@link org.greenrobot.greendao.AbstractDao#refresh(Object)}.
     * Entity must attached to an entity context.
     */
    @Generated(hash = 1942392019)
    public void refresh() {
        if (myDao == null) {
            throw new DaoException("Entity is detached from DAO context");
        }
        myDao.refresh(this);
    }

    /**
     * Convenient call for {@link org.greenrobot.greendao.AbstractDao#update(Object)}.
     * Entity must attached to an entity context.
     */
    @Generated(hash = 713229351)
    public void update() {
        if (myDao == null) {
            throw new DaoException("Entity is detached from DAO context");
        }
        myDao.update(this);
    }

    /**
     * called by internal mechanisms, do not call yourself.
     */
    @Generated(hash = 215161401)
    public void __setDaoSession(DaoSession daoSession) {
        this.daoSession = daoSession;
        myDao = daoSession != null ? daoSession.getMovieDao() : null;
    }

    public String getPoster_path_user() {
        return this.poster_path_user;
    }

    public void setPoster_path_user(String poster_path_user) {
        this.poster_path_user = poster_path_user;
    }

    public String getBackdrop_path_user() {
        return this.backdrop_path_user;
    }

    public void setBackdrop_path_user(String backdrop_path_user) {
        this.backdrop_path_user = backdrop_path_user;
    }
}
