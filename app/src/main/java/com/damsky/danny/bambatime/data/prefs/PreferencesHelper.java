package com.damsky.danny.bambatime.data.prefs;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatDelegate;

import java.util.Calendar;

/**
 * This is a helper class for dealing with the SharedPreferences throughout the application.
 *
 * @author Danny Damsky
 */

public final class PreferencesHelper {
    public static final String DEFAULT_PREFERENCE_IS_ADULT = "is_adult_preference";
    public static final String DEFAULT_PREFERENCE_NIGHT_MODE = "night_mode_preference";

    private static final String RUNTIME_PREFERENCES_NAME = "runtime_preferences";
    private static final String RUNTIME_PREFERENCE_FIRST_RUN = "first_run_preference";
    private static final String RUNTIME_PREFERENCE_LANG_CODE = "lang_code_preference";

    private static final String CACHE_PREFERENCES_NAME = "cache_preferences";
    private static final String CACHE_KEY_EXTENSION = "_last_updated=";
    private static final String CACHE_PREFERENCE_DISCOVER_JSON = "discover_json_preference";

    private static final Long UPDATE_INTERVAL = 259_200_000L; // 3 Days in Milliseconds.

    private final SharedPreferences defaultPreferences;
    private final SharedPreferences runtimePreferences;
    private final SharedPreferences cachePreferences;

    public PreferencesHelper(@NonNull Context context) {
        defaultPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        runtimePreferences = context.getSharedPreferences(RUNTIME_PREFERENCES_NAME, Context.MODE_PRIVATE);
        cachePreferences = context.getSharedPreferences(CACHE_PREFERENCES_NAME, Context.MODE_PRIVATE);
    }

    // Default Preferences

    /**
     * This function returns a string that is used to determine whether the user is an adult or not
     * in TMDB's API.
     *
     * @return "true" if true and "false" if false.
     */

    public String isAdult() {
        boolean isAdult = defaultPreferences.getBoolean(DEFAULT_PREFERENCE_IS_ADULT, false);
        if (isAdult)
            return "true";

        return "false";
    }

    /**
     * @return A constant value for either night mode enabled or disabled.
     */

    public int getTheme() {
        boolean isNightMode = defaultPreferences.getBoolean(DEFAULT_PREFERENCE_NIGHT_MODE, false);
        if (isNightMode)
            return AppCompatDelegate.MODE_NIGHT_YES;

        return AppCompatDelegate.MODE_NIGHT_NO;
    }

    // Runtime Preferences

    /**
     * @return The language code. (Usually the first two letters of the language's name)
     */
    private String getLangCode() {
        return runtimePreferences.getString(RUNTIME_PREFERENCE_LANG_CODE, "en");
    }

    /**
     * @param langCode Usually the first two letters of the language's name.
     */
    private void setLangCode(@NonNull String langCode) {
        SharedPreferences.Editor editor = runtimePreferences.edit();
        editor.putString(RUNTIME_PREFERENCE_LANG_CODE, langCode);
        editor.apply();
    }

    /**
     * @return If it's the first time that the user is running this app.
     */
    public boolean isFirstRun() {
        return runtimePreferences.getBoolean(RUNTIME_PREFERENCE_FIRST_RUN, true);
    }

    public void setIsNotFirstRun() {
        SharedPreferences.Editor editor = runtimePreferences.edit();
        editor.putBoolean(RUNTIME_PREFERENCE_FIRST_RUN, false);
        editor.apply();
    }

    /**
     * This checks to see if an update is required to the database.
     * It could be because the user changed the language of their phone or simply because
     * of the update interval.
     *
     * @param currentLangCode The language code of the device's current locale.
     * @return true if an update is required, otherwise false.
     */
    public boolean updateRequired(@NonNull String currentLangCode) {
        if (!currentLangCode.equals(getLangCode())) {
            clearCache();
            setLangCode(currentLangCode);
            return true;
        } else
            return getDiscoverCache() == null;
    }

    // Cache Preferences

    /**
     * Caching values instead of re-downloading them every time saves a lot of performance,
     * therefore it is very recommended that you use this function when Discover results are
     * changed.
     *
     * @param value The JSON of the first Discover page results.
     */
    public void setDiscoverCache(@NonNull String value) {
        insertCache(CACHE_PREFERENCE_DISCOVER_JSON, value);
    }

    /**
     * @return The JSON of the first Discover page results.
     */
    public String getDiscoverCache() {
        return getCache(CACHE_PREFERENCE_DISCOVER_JSON);
    }

    /**
     * This function allows caching JSONs to the SharedPreferences to avoid constantly having
     * to download them during runtime.
     * <p>
     * Inserting cache via this function means that the cache will be obsolete after the update
     * interval passes OR if the user decides to change the language of their phone.
     *
     * @param key   A key String, for example: User input.
     * @param value The value should be a JSON of movie results.
     */
    public void insertCache(@NonNull String key, @NonNull String value) {
        SharedPreferences.Editor editor = cachePreferences.edit();
        editor.putString(key, value);
        editor.putLong(key + CACHE_KEY_EXTENSION, Calendar.getInstance().getTimeInMillis());
        editor.apply();
    }

    /**
     * @param key A key String, for example: User input.
     * @return The value if it's found (The value should be a JSON String), else null.
     */
    public String getCache(@NonNull String key) {
        Long today = Calendar.getInstance().getTimeInMillis();
        Long lastUpdated = cachePreferences.getLong(key + CACHE_KEY_EXTENSION, today);

        if (today - lastUpdated >= UPDATE_INTERVAL)
            return null;

        return cachePreferences.getString(key, null);
    }

    /**
     * Completely clears the Cache SharedPreferences.
     */
    private void clearCache() {
        SharedPreferences.Editor editor = cachePreferences.edit();
        editor.clear();
        editor.apply();
    }

    /**
     * Completely clears the cache except for the cache of the Discover page results.
     * This is required for removing caches of adult content if the user suddenly decides
     * to set the isAdult preference to false.
     * This way, the Discover page results are still available. (Because the Discover page
     * results are strictly being queried to never allow adult content in this app))
     */
    public void clearCacheExceptForDiscoverCache() {
        String discoverCache = getDiscoverCache();
        SharedPreferences.Editor editor = cachePreferences.edit();
        editor.clear();
        if (discoverCache != null)
            editor.putString(CACHE_PREFERENCE_DISCOVER_JSON, discoverCache);
        editor.apply();
    }
}
