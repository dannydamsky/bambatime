package com.damsky.danny.bambatime.ui.activity.about;

import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.widget.Button;

import com.damsky.danny.bambatime.R;
import com.damsky.danny.bambatime.util.Constants;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * This activity shows the "About" section of the application.
 *
 * @author Danny Damsky
 */

public final class AboutPageActivity extends AppCompatActivity {

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.versionButton)
    Button versionButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about_page);
        ButterKnife.bind(this);

        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        versionButton.append(" " + getString(R.string.versionName));
    }

    @Override
    public boolean onSupportNavigateUp() {
        finish();
        return true;
    }

    private Intent buildIntent(String intentAction, String destinationUri) {
        Intent intent = new Intent(intentAction);
        Uri data = Uri.parse(destinationUri);
        intent.setData(data);
        return intent;
    }

    @OnClick(R.id.sourceCodeButton)
    public void openSourceCode() {
        Intent openSourceCodeWebPage = buildIntent(Intent.ACTION_VIEW, Constants.URL_SOURCE_CODE);
        startActivity(openSourceCodeWebPage);
    }

    @OnClick(R.id.rateButton)
    public void openStorePage() {
        try {
            startStorePageInPlayStoreApp();
        } catch (ActivityNotFoundException e) {
            startStorePageInBrowser();
        }
    }

    private void startStorePageInPlayStoreApp() {
        Intent startPlayStorePage = buildIntent(Intent.ACTION_VIEW, Constants.URL_STORE);
        startActivity(startPlayStorePage);
    }

    private void startStorePageInBrowser() {
        Intent startPlayStoreWebPage = buildIntent(Intent.ACTION_VIEW, Constants.URL_STORE_BACKUP);
        startActivity(startPlayStoreWebPage);
    }

    @OnClick(R.id.emailButton)
    public void openEmailIntent() {
        Intent emailToAuthor = buildIntent(Intent.ACTION_SENDTO, Constants.URL_EMAIL);
        Intent chooser = Intent.createChooser(emailToAuthor, getString(R.string.send_mail_using));
        startActivity(chooser);
    }
}
