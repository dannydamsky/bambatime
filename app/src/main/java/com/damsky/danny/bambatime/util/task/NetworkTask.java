package com.damsky.danny.bambatime.util.task;

import android.os.AsyncTask;
import android.support.annotation.NonNull;
import android.util.Log;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * This task is used to download web page source code.
 *
 * @author Danny Damsky
 */

public final class NetworkTask extends AsyncTask<String, Void, String> {

    private String errorMessage;
    private int errors;
    private NetworkTaskCallback callback;

    public NetworkTask(@NonNull NetworkTaskCallback callback) {
        this.callback = callback;
    }

    private void addErrorMessage(Exception e) {
        errors++;
        errorMessage += "Error Message " + errors + ":\n" + e.getMessage() + "\n";
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        callback.onDownloadStarted();
    }

    /**
     * @param strings Should be only one string, and it should contain a URL.
     * @return A string with the source code of the given URL, or an error message if the task fails.
     */
    @Override
    protected String doInBackground(String... strings) {
        errors = 0;
        errorMessage = "";

        StringBuilder sourceCode = new StringBuilder();
        BufferedReader input = null;
        HttpURLConnection connection = null;
        InputStream inputStream = null;
        InputStreamReader inputStreamReader = null;

        try {
            URL url = new URL(strings[0]);
            connection = (HttpURLConnection) url.openConnection();

            inputStream = connection.getInputStream();
            inputStreamReader = new InputStreamReader(inputStream);
            input = new BufferedReader(inputStreamReader);

            String line;
            while ((line = input.readLine()) != null)
                sourceCode.append(line);

        } catch (Exception e) {
            addErrorMessage(e);
        } finally {
            if (connection != null)
                connection.disconnect();

            if (input != null) {
                try {
                    inputStreamReader.close();
                    inputStream.close();
                    input.close();
                } catch (Exception e) {
                    addErrorMessage(e);
                }
            }
        }

        return sourceCode.toString();
    }

    @Override
    protected void onPostExecute(String webSource) {
        super.onPostExecute(webSource);
        if (errorMessage.isEmpty())
            callback.onDownloadSucceeded(webSource);
        else {
            callback.onDownloadFailed();
            Log.e("ErrorInNetworkTask", errorMessage);
        }
        callback = null;
    }

    public interface NetworkTaskCallback {
        default void onDownloadStarted() {
        }

        void onDownloadSucceeded(String webSource);

        void onDownloadFailed();
    }
}
