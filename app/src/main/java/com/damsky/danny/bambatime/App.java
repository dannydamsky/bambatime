package com.damsky.danny.bambatime;

import android.app.Application;
import android.support.v7.app.AppCompatDelegate;

import com.damsky.danny.bambatime.data.localdb.DBHelper;
import com.damsky.danny.bambatime.data.prefs.PreferencesHelper;

/**
 * Using this class allows casting it to getApplication() in Android classes that implement Context.
 * This way the properties that it contains are available in the entire application and do not need
 * to be re-instantiated elsewhere.
 *
 * @author Danny Damsky
 */

public final class App extends Application {

    private DBHelper dbHelper;
    private PreferencesHelper preferencesHelper;

    @Override
    public void onCreate() {
        super.onCreate();
        dbHelper = new DBHelper(this);
        preferencesHelper = new PreferencesHelper(this);
        AppCompatDelegate.setDefaultNightMode(preferencesHelper.getTheme());
    }

    public DBHelper getDbHelper() {
        return dbHelper;
    }

    public PreferencesHelper getPreferencesHelper() {
        return preferencesHelper;
    }
}
