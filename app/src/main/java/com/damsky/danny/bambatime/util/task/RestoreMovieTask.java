package com.damsky.danny.bambatime.util.task;

import android.os.AsyncTask;
import android.support.annotation.NonNull;
import android.support.v4.util.Pair;
import android.util.Log;

import com.damsky.danny.bambatime.data.localdb.model.Movie;
import com.damsky.danny.bambatime.util.Constants;

import org.json.JSONObject;

/**
 * This task is used to revert user-made changes to the movie.
 *
 * @author Danny Damsky
 */

public final class RestoreMovieTask extends AsyncTask<Pair<String, Movie>, Void, Void> {

    private RestoreMovieTaskCallback callback;
    private boolean errorHappened = false;

    public RestoreMovieTask(@NonNull RestoreMovieTaskCallback callback) {
        this.callback = callback;
    }

    /**
     * @param pairs There should only be one object, the first value should be a String containing
     *              a JSON of the movie. The second value should be an object of said movie.
     * @return Nothing.
     */
    @Override
    protected Void doInBackground(Pair<String, Movie>[] pairs) {
        try {
            Pair<String, Movie> pair = pairs[0];
            JSONObject thisResult = new JSONObject(pair.first);

            String title = thisResult.getString(Constants.MOVIE_TITLE);
            String overview = thisResult.getString(Constants.MOVIE_OVERVIEW);
            String release_date = thisResult.getString(Constants.MOVIE_RELEASE_DATE);
            double vote_average = thisResult.getDouble(Constants.MOVIE_VOTE_AVERAGE);
            String tagline = thisResult.getString(Constants.MOVIE_TAGLINE);
            int budget = thisResult.getInt(Constants.MOVIE_BUDGET);
            int revenue = thisResult.getInt(Constants.MOVIE_REVENUE);

            int runtime;
            try {
                runtime = thisResult.getInt(Constants.MOVIE_RUNTIME);
            } catch (Exception e) {
                runtime = -1;
            }

            String status = thisResult.getString(Constants.MOVIE_STATUS);

            if (pair.second != null) {
                pair.second.setTitle(title);
                pair.second.setOverview(overview);
                pair.second.setRelease_date(release_date);
                pair.second.setVote_average(vote_average);
                pair.second.setTagline(tagline);
                pair.second.setBudget(budget);
                pair.second.setRevenue(revenue);
                pair.second.setRuntime(runtime);
                pair.second.setStatus(status);
                pair.second.setModifiable(false);
                pair.second.setPoster_path_user(null);
                pair.second.setBackdrop_path_user(null);
            }
        } catch (Exception e) {
            // This should never happen. If it does, the JSON was incorrect.
            Log.e("ErrorInRestoreMovieTask", e.getMessage());
            errorHappened = true;
        }

        return null;
    }

    @Override
    protected void onPostExecute(Void aVoid) {
        super.onPostExecute(aVoid);
        if (!errorHappened)
            callback.onRestoreMovieSucceeded();

        callback = null;
    }

    public interface RestoreMovieTaskCallback {
        void onRestoreMovieSucceeded();
    }
}
