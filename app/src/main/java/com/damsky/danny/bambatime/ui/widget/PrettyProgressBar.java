package com.damsky.danny.bambatime.ui.widget;

import android.content.Context;
import android.support.constraint.ConstraintLayout;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.damsky.danny.bambatime.R;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * This View contains a circular ProgressBar and a TextView inside it indicating the percentage.
 *
 * @author Danny Damsky
 * @see ProgressBar
 * @see TextView
 */
public final class PrettyProgressBar extends ConstraintLayout {

    @BindView(R.id.progress)
    ProgressBar progress;
    @BindView(R.id.percent)
    TextView percent;

    {
        LayoutInflater layoutInflater = LayoutInflater.from(getContext());
        View thisView = layoutInflater.inflate(R.layout.progressbar_with_text, this, true);
        ButterKnife.bind(this, thisView);
    }

    public PrettyProgressBar(Context context) {
        super(context);
    }

    public PrettyProgressBar(Context context, AttributeSet attrs) {
        super(context, attrs);
    }


    public PrettyProgressBar(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    /**
     * Sets the current progress to the specified value.
     *
     * @param progress the new progress, between 0 and 100.
     */
    public final void setProgress(int progress) {
        this.progress.setProgress(progress);
        this.percent.setText(progress + "%");
    }
}
