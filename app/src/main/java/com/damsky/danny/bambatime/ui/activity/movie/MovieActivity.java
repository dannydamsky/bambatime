package com.damsky.danny.bambatime.ui.activity.movie;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.constraint.Group;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.FileProvider;
import android.support.v4.util.Pair;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.TextView;
import android.widget.Toast;

import com.damsky.danny.bambatime.App;
import com.damsky.danny.bambatime.GlideApp;
import com.damsky.danny.bambatime.R;
import com.damsky.danny.bambatime.data.localdb.model.Genre;
import com.damsky.danny.bambatime.data.localdb.model.Movie;
import com.damsky.danny.bambatime.ui.activity.movie.adapter.GenreCardAdapter;
import com.damsky.danny.bambatime.ui.dialog.alertdialog.SimpleAlertDialog;
import com.damsky.danny.bambatime.ui.dialog.imagechoose.ImageChooseDialog;
import com.damsky.danny.bambatime.ui.dialog.moviedetails.BundleHolder;
import com.damsky.danny.bambatime.ui.dialog.moviedetails.MovieDetailsDialog;
import com.damsky.danny.bambatime.ui.widget.PrettyProgressBar;
import com.damsky.danny.bambatime.util.Constants;
import com.damsky.danny.bambatime.util.JSONFormatConverter;
import com.damsky.danny.bambatime.util.task.NetworkTask;
import com.damsky.danny.bambatime.util.task.RestoreMovieTask;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * This activity presents the user with full information about a movie.
 * It also allows the user to edit the movie, bookmark it and revert the changes that were made.
 *
 * @author Danny Damsky
 * @see GenreCardAdapter
 * @see MovieDetailsDialog
 * @see ImageChooseDialog
 * @see SimpleAlertDialog
 * @see NetworkTask
 * @see RestoreMovieTask
 * @see PrettyProgressBar
 */

public final class MovieActivity extends AppCompatActivity implements GenreCardAdapter.OnCardClickListener,
        PopupMenu.OnMenuItemClickListener, MovieDetailsDialog.OnMovieDetailsDialogDismissListener,
        ImageChooseDialog.OnImageChooseDialogDismissListener, SimpleAlertDialog.OnAlertDialogDismissListener {

    private Movie movie;
    private App appReference;

    @BindView(R.id.container)
    CoordinatorLayout container;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.movieBackdrop)
    ImageView movieBackdrop;
    @BindView(R.id.moviePoster)
    ImageView moviePoster;
    @BindView(R.id.tagline)
    TextView tagline;
    @BindView(R.id.original_language)
    TextView original_language;
    @BindView(R.id.original_title)
    TextView original_title;
    @BindView(R.id.overview)
    TextView overview;
    @BindView(R.id.genres)
    RecyclerView genres;
    @BindView(R.id.budget)
    TextView budget;
    @BindView(R.id.revenue)
    TextView revenue;
    @BindView(R.id.runtime)
    TextView runtime;
    @BindView(R.id.status)
    TextView status;
    @BindView(R.id.progressBar)
    PrettyProgressBar progressBar;
    @BindView(R.id.menu)
    ImageButton menu;

    @BindView(R.id.originalsGroup)
    Group originalsGroup;
    @BindView(R.id.overviewsGroup)
    Group overviewsGroup;
    @BindView(R.id.genresGroup)
    Group genresGroup;
    @BindView(R.id.budgetsGroup)
    Group budgetsGroup;
    @BindView(R.id.revenuesGroup)
    Group revenuesGroup;
    @BindView(R.id.runtimesGroup)
    Group runtimesGroup;
    @BindView(R.id.statusesGroup)
    Group statusesGroup;

    private PopupMenu popupMenu;
    private MenuItem action_revert;

    private String keyForBundle;
    private Bundle dialogBundle;
    private String mCurrentPhotoPath;

    private NetworkTask sourceCodeLoaderTask;
    private RestoreMovieTask movieTask;

    /* This activity runs if the user pressed "Revert Changes"
       It queries for the movie in the TMDB API, downloads the JSON, parses it and fills in the
       movie's original properties. */

    private void asyncRestoreMovie() {
        sourceCodeLoaderTask = new NetworkTask(new NetworkTask.NetworkTaskCallback() {
            @Override
            public void onDownloadStarted() {
                Snackbar.make(container, R.string.revering_changes_message, Snackbar.LENGTH_INDEFINITE).show();
            }

            @SuppressWarnings("unchecked")
            @Override
            public void onDownloadSucceeded(String webSource) {
                Pair<String, Movie> passToTask = new Pair<>(webSource, movie);

                movieTask = new RestoreMovieTask(() -> {
                    getIntent().putExtra(Constants.EXTRA_RESTART_SOURCE_CODE_LOADER_TASK, false);
                    appReference.getDbHelper().updateMovie(movie);
                    Snackbar.make(container, R.string.success_reverting_changes, Snackbar.LENGTH_SHORT).show();

                    action_revert.setVisible(false);
                    initialize();
                });

                movieTask.execute(passToTask);
            }

            @Override
            public void onDownloadFailed() {
                Snackbar.make(container, R.string.error_reload, Snackbar.LENGTH_LONG).show();
            }
        });

        sourceCodeLoaderTask.execute(getString(R.string.tmdb_movies_query, movie.getTmdbId()));
    }

    private void setBackdropImage() {
        Bitmap backdropImage = BitmapFactory.decodeFile(movie.getBackdrop_path_user());
        if (backdropImage != null)
            GlideApp.with(this)
                    .load(backdropImage)
                    .placeholder(R.drawable.placeholder_no_image_available_backdrop)
                    .into(movieBackdrop);
        else
            GlideApp.with(this)
                    .load(getString(R.string.tmdb_image_url, movie.getBackdrop_path()))
                    .placeholder(R.drawable.placeholder_no_image_available_backdrop)
                    .into(movieBackdrop);
    }

    private void setPosterImage() {
        Bitmap posterImage = BitmapFactory.decodeFile(movie.getPoster_path_user());
        if (posterImage != null)
            GlideApp.with(this)
                    .load(posterImage)
                    .placeholder(R.drawable.placeholder_no_image_available_poster)
                    .into(moviePoster);
        else
            GlideApp.with(this)
                    .load(getString(R.string.tmdb_image_url, movie.getPoster_path()))
                    .placeholder(R.drawable.placeholder_no_image_available_poster)
                    .into(moviePoster);
    }

    private void setOnMenuImageButtonPressed() {
        popupMenu = new PopupMenu(this, menu);
        popupMenu.inflate(R.menu.menu_movie);
        popupMenu.setOnMenuItemClickListener(this);
        action_revert = popupMenu.getMenu().findItem(R.id.action_revert);

        if (movie.getBookmarked())
            popupMenu.getMenu().findItem(R.id.action_bookmark).setTitle(R.string.bookmarks_remove);

        if (movie.getTmdbId() == null || !movie.getModifiable())
            action_revert.setVisible(false);

        if (movie.getHomepage() == null || movie.getHomepage().isEmpty())
            popupMenu.getMenu().findItem(R.id.action_homepage).setVisible(false);

        if (movie.getTmdbId() == null)
            popupMenu.getMenu().findItem(R.id.action_tmdb_page).setVisible(false);

        if (movie.getImdb_id() == null || movie.getImdb_id().isEmpty())
            popupMenu.getMenu().findItem(R.id.action_imdb_page).setVisible(false);

        menu.setOnClickListener(v -> popupMenu.show());
    }

    private void setProgress() {
        int percentValue = JSONFormatConverter.getVoteAverageValueInPercent(movie.getVote_average());
        progressBar.setProgress(percentValue);
    }

    private void setTagLine() {
        if (movie.getTagline() != null) {
            tagline.setVisibility(View.VISIBLE);
            tagline.setText(movie.getTagline());
        } else
            tagline.setVisibility(View.GONE);
    }

    private void setOriginals() {
        if (movie.getOriginal_title() != null && !movie.getOriginal_title().equals(movie.getTitle())) {
            originalsGroup.setVisibility(View.VISIBLE);
            original_language.setText(JSONFormatConverter.getLanguage(movie.getOriginal_language()));
            original_title.setText(movie.getOriginal_title());
        } else
            originalsGroup.setVisibility(View.GONE);
    }

    private void setOverview() {
        if (!movie.getOverview().isEmpty()) {
            overviewsGroup.setVisibility(View.VISIBLE);
            overview.setText(movie.getOverview());
        } else
            overviewsGroup.setVisibility(View.GONE);
    }

    private void setGenres() {
        List<Genre> genreList = movie.getGenres();
        if (genreList != null && !genreList.isEmpty()) {
            genresGroup.setVisibility(View.VISIBLE);
            genres.setAdapter(new GenreCardAdapter(this, genreList));
        } else
            genresGroup.setVisibility(View.GONE);
    }

    private void setBudget() {
        if (movie.getBudget() > 0) {
            budgetsGroup.setVisibility(View.VISIBLE);
            budget.setText(JSONFormatConverter.getFormattedMoneyString(movie.getBudget()));
        } else
            budgetsGroup.setVisibility(View.GONE);
    }

    private void setRevenue() {
        if (movie.getRevenue() > 0) {
            revenuesGroup.setVisibility(View.VISIBLE);
            revenue.setText(JSONFormatConverter.getFormattedMoneyString(movie.getRevenue()));
        } else
            revenuesGroup.setVisibility(View.GONE);
    }

    private void setRuntime() {
        if (movie.getRuntime() > 0) {
            runtimesGroup.setVisibility(View.VISIBLE);
            runtime.setText(JSONFormatConverter.getFormattedRuntime(this, movie.getRuntime()));
        } else
            runtimesGroup.setVisibility(View.GONE);
    }

    private void setStatus() {
        if (!movie.getStatus().isEmpty()) {
            statusesGroup.setVisibility(View.VISIBLE);
            status.setText(movie.getStatus());
        } else
            statusesGroup.setVisibility(View.GONE);
    }

    private void setToolbarTitles() {
        toolbar.setTitle(movie.getTitle());
        toolbar.setSubtitle(JSONFormatConverter.getFormattedReleaseDate(this, movie.getRelease_date()));
        setSupportActionBar(toolbar);
    }

    private void initialize() {
        setBackdropImage();
        setPosterImage();
        setToolbarTitles();
        setProgress();
        setTagLine();
        setOriginals();
        setOverview();
        setGenres();
        setBudget();
        setRevenue();
        setRuntime();
        setStatus();
    }

    private boolean taskRunning(AsyncTask task) {
        return task != null && task.getStatus() == AsyncTask.Status.RUNNING;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_movie);
        ButterKnife.bind(this);

        genres.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
        genres.setItemAnimator(new DefaultItemAnimator());

        appReference = (App) getApplication();
        movie = appReference.getDbHelper().getMovieById(getIntent().getLongExtra(Constants.EXTRA_MOVIE_ID, -1));
        movie.setIs_seen(true);
        appReference.getDbHelper().updateMovie(movie);

        setBackdropImage();
        setPosterImage();
        setOnMenuImageButtonPressed();
        initialize();
    }

    // What happens when a genre card was clicked?
    // This isn't implemented yet, but could be useful for later.
    @Override
    public void onCardClicked(int position) {
    }

    @Override
    public void finish() {
        if (!((sourceCodeLoaderTask != null && sourceCodeLoaderTask.getStatus() == AsyncTask.Status.RUNNING) ||
                (movieTask != null && movieTask.getStatus() == AsyncTask.Status.RUNNING))) {
            setResult(RESULT_OK, getIntent());
            super.finish();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (getIntent().getBooleanExtra(Constants.EXTRA_RESTART_SOURCE_CODE_LOADER_TASK, false))
            asyncRestoreMovie();
    }

    @Override
    protected void onPause() {
        if (taskRunning(sourceCodeLoaderTask)) {
            sourceCodeLoaderTask.cancel(true);
            getIntent().putExtra(Constants.EXTRA_RESTART_SOURCE_CODE_LOADER_TASK, true);
        }

        if (taskRunning(movieTask))
            movieTask.cancel(true);

        if (dialogBundle != null)
            getIntent().putExtra(Constants.EXTRA_BUNDLE, dialogBundle);

        super.onPause();
    }

    @OnClick(R.id.back)
    public void backImageButtonPressed() {
        finish();
    }

    @Override
    public boolean onMenuItemClick(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_bookmark:
                if (movie.getBookmarked()) {
                    movie.setBookmarked(false);
                    item.setTitle(R.string.bookmarks_add);
                } else {
                    movie.setBookmarked(true);
                    item.setTitle(R.string.bookmarks_remove);
                }
                appReference.getDbHelper().updateMovie(movie);
                break;

            case R.id.action_revert:
                SimpleAlertDialog.show(getSupportFragmentManager(), R.string.confirm_action, R.string.revert_changes_question);
                break;

            case R.id.action_edit:
                MovieDetailsDialog.show(getSupportFragmentManager(), dialogBundle, movie);
                break;

            case R.id.action_homepage:
                startWebsite(movie.getHomepage());
                break;

            case R.id.action_imdb_page:
                startWebsite(getString(R.string.url_imdb_page, movie.getImdb_id()));
                break;

            case R.id.action_tmdb_page:
                startWebsite(getString(R.string.url_tmdb_page, movie.getTmdbId()));
                break;
        }
        return true;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case Constants.REQUEST_CODE_GALLERY:
                if (data != null)
                    dialogBundle.putString(keyForBundle, getPath(data.getData()));
                MovieDetailsDialog.show(getSupportFragmentManager(), dialogBundle, movie);
                break;

            case Constants.REQUEST_CODE_CAMERA:
                Bitmap bitmap = BitmapFactory.decodeFile(mCurrentPhotoPath);

                if (bitmap != null)
                    dialogBundle.putString(keyForBundle, mCurrentPhotoPath);

                mCurrentPhotoPath = null;
                MovieDetailsDialog.show(getSupportFragmentManager(), dialogBundle, movie);
                break;
        }
    }

    @Override
    public void onImageChooseDialogCameraOption() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        // Ensure that there's a camera activity to handle the intent
        if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
            // Create the File where the photo should go
            File photoFile = null;
            try {
                photoFile = createImageFile();
            } catch (IOException ex) {
                // Error occurred while creating the File
                Toast.makeText(this, R.string.error_creating_image, Toast.LENGTH_SHORT).show();
            }
            // Continue only if the File was successfully created
            if (photoFile != null) {
                Uri photoURI = FileProvider.getUriForFile(this,
                        getApplicationContext().getPackageName() + ".fileprovider",
                        photoFile);
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                startActivityForResult(takePictureIntent, Constants.REQUEST_CODE_CAMERA);
            }
        }

    }

    @Override
    public void onImageChooseDialogCancelled() {
        try {
            if (dialogBundle == null)
                dialogBundle = getIntent().getBundleExtra(Constants.EXTRA_BUNDLE);

            MovieDetailsDialog.show(getSupportFragmentManager(), dialogBundle, movie);
            getIntent().removeExtra(Constants.EXTRA_BUNDLE);
        } catch (Exception e) {
            // This happens when application rotates, this isn't an error for checked cases.
            Log.d("ErrorInMovieActivity", e.getMessage());
        }
    }

    @Override
    public void onImageChooseDialogGalleryOption() {
        Intent pickImageIntent = new Intent(Intent.ACTION_PICK);
        pickImageIntent.setType("image/*");
        startActivityForResult(Intent.createChooser(pickImageIntent, getString(R.string.select_picture)),
                Constants.REQUEST_CODE_GALLERY);
    }

    @Override
    public void onMovieDetailsDialogDismissed(int resultCode, Bundle bundle) {
        switch (resultCode) {
            case Constants.RESULT_CODE_POSTER:
                dialogBundle = bundle;
                keyForBundle = Constants.MOVIE_POSTER_PATH_USER;
                requestPermissionsForGettingImages();
                break;

            case Constants.RESULT_CODE_BACKDROP:
                dialogBundle = bundle;
                keyForBundle = Constants.MOVIE_BACKDROP_PATH_USER;
                requestPermissionsForGettingImages();
                break;

            case Constants.RESULT_CODE_DIALOG_OK:
                BundleHolder bundleHolder = new BundleHolder(bundle);
                bundleHolder.updateMovie(movie);
                appReference.getDbHelper().updateMovie(movie);

                if (movie.getTmdbId() != null)
                    action_revert.setVisible(true);

                initialize();
                keyForBundle = null;
                dialogBundle = null;
                break;
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case Constants.REQUEST_CODE_PERMISSIONS_WRITE_EXTERNAL_STORAGE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED)
                    ImageChooseDialog.show(getSupportFragmentManager());
                else
                    MovieDetailsDialog.show(getSupportFragmentManager(), dialogBundle, movie);
                break;
        }
    }

    @Override
    public void onAlertDialogOkPressed() {
        asyncRestoreMovie();
    }

    private void requestPermissionsForGettingImages() {
        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {

            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                    Constants.REQUEST_CODE_PERMISSIONS_WRITE_EXTERNAL_STORAGE);
        } else
            ImageChooseDialog.show(getSupportFragmentManager());
    }

    @SuppressLint("SimpleDateFormat")
    private File createImageFile() throws IOException {
        // Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        File storageDir = getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(
                imageFileName,  /* prefix */
                ".jpg",         /* suffix */
                storageDir      /* directory */
        );

        // Save a file: path for use with ACTION_VIEW intents
        mCurrentPhotoPath = image.getAbsolutePath();
        return image;
    }

    private String getPath(Uri uri) {
        String column = MediaStore.Images.Media.DATA;
        String[] projection = {column};

        Cursor cursor = getContentResolver().query(uri, projection, null, null, null);
        if (cursor != null) {
            int column_index = cursor.getColumnIndex(MediaStore.Images.Media.DATA);
            cursor.moveToNext();
            String path = cursor.getString(column_index);
            cursor.close();
            return path;
        }

        return null;
    }

    private void startWebsite(String url) {
        Intent websiteIntent = new Intent(Intent.ACTION_VIEW);
        websiteIntent.setData(Uri.parse(url));
        startActivity(websiteIntent);
    }
}
