package com.damsky.danny.bambatime.ui.activity.search;

import android.app.SearchManager;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.provider.SearchRecentSuggestions;
import android.support.annotation.NonNull;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.v4.util.Pair;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.app.AppCompatDelegate;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.Surface;
import android.view.View;
import android.view.WindowManager;
import android.widget.TextView;

import com.damsky.danny.bambatime.App;
import com.damsky.danny.bambatime.R;
import com.damsky.danny.bambatime.data.localdb.model.Movie;
import com.damsky.danny.bambatime.provider.SearchSuggestionsProvider;
import com.damsky.danny.bambatime.ui.activity.about.AboutPageActivity;
import com.damsky.danny.bambatime.ui.activity.bookmarks.BookmarkedMoviesActivity;
import com.damsky.danny.bambatime.ui.activity.main.MainActivity;
import com.damsky.danny.bambatime.ui.activity.main.adapter.MainRecycleAdapter;
import com.damsky.danny.bambatime.ui.activity.movie.MovieActivity;
import com.damsky.danny.bambatime.ui.activity.mymovies.MyMoviesActivity;
import com.damsky.danny.bambatime.ui.activity.prefs.PreferencesActivity;
import com.damsky.danny.bambatime.util.Constants;
import com.damsky.danny.bambatime.util.task.MovieQueryTask;
import com.damsky.danny.bambatime.util.task.NetworkTask;
import com.damsky.danny.bambatime.util.task.SearchTask;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.List;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * This activity uses the same layout as the MainActivity.
 * The purpose of this activity is to provide movie search results to user input.
 *
 * @author Danny Damsky
 * @see MainActivity
 * @see MainRecycleAdapter
 * @see NetworkTask
 * @see SearchTask
 * @see MovieQueryTask
 */

public final class SearchActivity extends AppCompatActivity implements
        NavigationView.OnNavigationItemSelectedListener, MainRecycleAdapter.OnCardClickListener,
        SearchView.OnQueryTextListener {

    private List<Movie> movies;
    private App appReference;

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.mainRecyclerView)
    RecyclerView mainRecyclerView;
    @BindView(R.id.drawerLayout)
    DrawerLayout drawerLayout;
    @BindView(R.id.container)
    CoordinatorLayout container;
    @BindView(R.id.navigationDrawer)
    NavigationView navigationDrawer;
    @BindView(R.id.emptyError)
    TextView emptyError;

    private Snackbar loading;
    private SearchView searchView;

    private NetworkTask searchJsonTask;
    private NetworkTask movieJsonTask;
    private SearchTask movieLoadTask;
    private MovieQueryTask movieQueryTask;

    private boolean shouldEnableBigLayout;

    private boolean doubleTapToExit = false;

    /* Uses TMDB API to search for movies that have a similar name to the parameter.
       There's also some caching going on,
       SharedPreferences is used to hold JSON values to user input keys */

    private void asyncSearch(String newText) {
        String cache = appReference.getPreferencesHelper().getCache(newText);
        if (cache == null) {
            searchJsonTask = new NetworkTask(new NetworkTask.NetworkTaskCallback() {
                @Override
                public void onDownloadStarted() {
                    loading.show();
                }

                @Override
                public void onDownloadSucceeded(String webSource) {
                    if (movieLoadTask != null && movieLoadTask.getStatus() == AsyncTask.Status.RUNNING)
                        movieLoadTask.cancel(true);
                    asyncLoadMovies(webSource);

                    appReference.getPreferencesHelper().insertCache(newText, webSource);
                }

                @Override
                public void onDownloadFailed() {
                    Snackbar.make(container, R.string.error_network, Snackbar.LENGTH_SHORT).show();
                }
            });
            try {
                searchJsonTask.execute(getString(R.string.tmdb_movies_search,
                        appReference.getPreferencesHelper().isAdult(),
                        URLEncoder.encode(newText, "UTF-8")));
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
        } else
            asyncLoadMovies(cache);
    }

    /* This method gets called from asyncSearch() after it's done finding the JSON of movie results.
       This method parses that JSON and sets the adapter for the recycler view, so when this method
       finishes executing the user will see a list of movies. */

    private void asyncLoadMovies(String webSource) {
        movieLoadTask = new SearchTask(simpleMovies -> {

            if (movieJsonTask != null && movieJsonTask.getStatus() == AsyncTask.Status.RUNNING)
                movieJsonTask.cancel(true);

            if (movieQueryTask != null && movieQueryTask.getStatus() == AsyncTask.Status.RUNNING)
                movieQueryTask.cancel(true);

            movies = simpleMovies;
            mainRecyclerView.setAdapter(new MainRecycleAdapter(this, movies, shouldEnableBigLayout));

            if (movies.size() == 0) {
                emptyError.setVisibility(View.VISIBLE);
                Snackbar.make(container, R.string.no_movies_found, Snackbar.LENGTH_SHORT).show();
            } else {
                emptyError.setVisibility(View.GONE);
                loading.dismiss();
            }

            asyncReadyMovies(0);
        });

        movieLoadTask.execute(webSource);
    }

    /* This method gets called from asyncLoadMovies() after it's done parsing the JSON of movie results.
       This method is recursive, in the sense that it calls itself after a certain action is done.
       This method is in charge of completing the movies (I.E. "readying" them), it queries each
       movie from the available list using the TMDB API (or gets the movie if it's already available
       in the DB) and adds all the missing fields to the appropriate object after it's done parsing
       the JSON of the query.
     */

    private void asyncReadyMovies(int movieIndex) {
        if (movieIndex < movies.size()) {
            Movie movie = movies.get(movieIndex);
            Movie getMovie = appReference.getDbHelper().getMovieByTmdbId(movie.getTmdbId());

            if (getMovie != null && (getMovie.getTitle().equals(movie.getTitle()) || getMovie.getModifiable())) {
                movies.set(movieIndex, getMovie);
                asyncReadyMovies(movieIndex + 1);
            } else {
                movieJsonTask = new NetworkTask(new NetworkTask.NetworkTaskCallback() {
                    @SuppressWarnings("unchecked")
                    @Override
                    public void onDownloadSucceeded(String webSource) {
                        Pair<String, Movie> passToTask = new Pair<>(webSource, movie);

                        movieQueryTask = new MovieQueryTask(genres -> {
                            appReference.getDbHelper().insertMovieWithGenres(movie, genres);
                            asyncReadyMovies(movieIndex + 1);
                        });

                        movieQueryTask.execute(passToTask);
                    }

                    @Override
                    public void onDownloadFailed() {
                        asyncReadyMovies(movieIndex + 1);
                    }
                });

                movieJsonTask.execute(getString(R.string.tmdb_movies_query, movie.getTmdbId()));
            }
        } else
            mainRecyclerView.getAdapter().notifyDataSetChanged();
    }

    private boolean isLandscape() {
        int rotationDegrees = ((WindowManager) Objects.requireNonNull(getSystemService(WINDOW_SERVICE)))
                .getDefaultDisplay()
                .getRotation();

        return rotationDegrees == Surface.ROTATION_90 || rotationDegrees == Surface.ROTATION_270;
    }

    private boolean isLargeScreen() {
        int screenSize = getResources().getConfiguration().screenLayout &
                Configuration.SCREENLAYOUT_SIZE_MASK;

        return screenSize == Configuration.SCREENLAYOUT_SIZE_LARGE ||
                screenSize == Configuration.SCREENLAYOUT_SIZE_XLARGE;
    }

    private boolean taskRunning(AsyncTask task) {
        return task != null && task.getStatus() == AsyncTask.Status.RUNNING;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        appReference = (App) getApplication();
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        loading = Snackbar.make(container, R.string.loading_movies_extensive, Snackbar.LENGTH_INDEFINITE);

        setSupportActionBar(toolbar);

        ActionBarDrawerToggle mToggle = new ActionBarDrawerToggle(this, drawerLayout, toolbar,
                R.string.open_nav_drawer, R.string.close_nav_drawer);

        drawerLayout.addDrawerListener(mToggle);
        mToggle.syncState();

        navigationDrawer.setNavigationItemSelectedListener(this);
        navigationDrawer.getMenu().findItem(R.id.action_search).setChecked(true);

        shouldEnableBigLayout = isLandscape() || isLargeScreen();
        if (shouldEnableBigLayout)
            mainRecyclerView.setLayoutManager(new GridLayoutManager(this, 2));
        else
            mainRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        mainRecyclerView.setItemAnimator(new DefaultItemAnimator());

        emptyError.setVisibility(View.VISIBLE);
        emptyError.setText(R.string.error_empty_search);
    }

    private void handleIntent(Intent intent) {
        if (Intent.ACTION_SEARCH.equals(intent.getAction())) {
            String query = intent.getStringExtra(SearchManager.QUERY);
            SearchRecentSuggestions suggestions = new SearchRecentSuggestions(this,
                    SearchSuggestionsProvider.AUTHORITY, SearchSuggestionsProvider.MODE);
            suggestions.saveRecentQuery(query, null);
            searchView.setQuery(query, false);
        }
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        handleIntent(intent);
    }

    @Override
    protected void onResume() {
        super.onResume();
        String newText = getIntent().getStringExtra(Constants.EXTRA_SEARCH_INPUT);
        if (newText != null)
            asyncSearch(newText);
    }

    @Override
    protected void onPause() {
        if (taskRunning(searchJsonTask))
            searchJsonTask.cancel(true);

        if (taskRunning(movieLoadTask))
            movieLoadTask.cancel(true);

        if (taskRunning(movieJsonTask))
            movieJsonTask.cancel(true);

        if (taskRunning(movieQueryTask))
            movieQueryTask.cancel(true);

        super.onPause();
    }

    @Override
    public void onBackPressed() {
        if (drawerLayout.isDrawerOpen(navigationDrawer))
            drawerLayout.closeDrawer(navigationDrawer);

        else if (doubleTapToExit)
            super.onBackPressed();

        else {
            final Snackbar snackbar = Snackbar
                    .make(container, R.string.double_tap_message, Snackbar.LENGTH_INDEFINITE);
            snackbar.show();
            doubleTapToExit = true;
            new Handler().postDelayed(() -> {
                doubleTapToExit = false;
                snackbar.dismiss();
            }, Constants.BACK_BUTTON_DOUBLE_TAP_INTERVAL);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_search, menu);

        SearchManager searchManager = (SearchManager) getSystemService(SEARCH_SERVICE);
        if (searchManager != null) {
            searchView = (SearchView) menu.findItem(R.id.item_search).getActionView();
            searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));
            searchView.setOnQueryTextListener(this);
        }

        return true;
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_settings:
                Intent startPrefs = new Intent(this, PreferencesActivity.class);
                startPrefs.putExtra(Constants.EXTRA_NIGHT_MODE_CHANGED, false);
                startActivityForResult(startPrefs, Constants.REQUEST_CODE_PREFERENCES);
                break;

            case R.id.action_my_movies:
                Intent startMyMovies = new Intent(this, MyMoviesActivity.class);
                startActivity(startMyMovies);
                finish();
                break;

            case R.id.action_bookmarked:
                Intent startBookmarkedMovies = new Intent(this, BookmarkedMoviesActivity.class);
                startActivity(startBookmarkedMovies);
                finish();
                break;

            case R.id.action_discover:
                Intent startMain = new Intent(this, MainActivity.class);
                startActivity(startMain);
                finish();
                break;

            case R.id.action_about:
                Intent startAbout = new Intent(this, AboutPageActivity.class);
                startActivity(startAbout);
                break;
        }

        drawerLayout.closeDrawer(navigationDrawer);
        return true;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case Constants.REQUEST_CODE_PREFERENCES:
                if (data != null) {
                    boolean adultSettingChanged = data.getBooleanExtra(Constants.EXTRA_ADULT_SETTING_CHANGED, false);
                    boolean nightModeChanged = data.getBooleanExtra(Constants.EXTRA_NIGHT_MODE_CHANGED, false);

                    if (nightModeChanged) {
                        AppCompatDelegate.setDefaultNightMode(appReference.getPreferencesHelper().getTheme());
                        if (adultSettingChanged)
                            getIntent().removeExtra(Constants.EXTRA_SEARCH_INPUT);
                        this.recreate();
                    } else if (adultSettingChanged) {
                        getIntent().removeExtra(Constants.EXTRA_SEARCH_INPUT);
                        this.recreate();
                    }
                }
                break;

            case Constants.REQUEST_CODE_MOVIE_ACTIVITY:
                if (movies != null) {
                    int positionInList = data.getIntExtra(Constants.EXTRA_MOVIE_POSITION_IN_LIST, -1);
                    Long movieId = movies.get(positionInList).getId();
                    movies.set(positionInList, appReference.getDbHelper().getMovieById(movieId));
                    mainRecyclerView.getAdapter().notifyDataSetChanged();
                }
                break;
        }
    }

    @Override
    public void onCardClicked(int position) {
        Movie movieClicked = movies.get(position);
        if (movieClicked.isReady()) {
            Intent intent = new Intent(this, MovieActivity.class);
            intent.putExtra(Constants.EXTRA_MOVIE_ID, movieClicked.getId());
            intent.putExtra(Constants.EXTRA_MOVIE_POSITION_IN_LIST, position);
            startActivityForResult(intent, Constants.REQUEST_CODE_MOVIE_ACTIVITY);
        } else
            Snackbar.make(container, R.string.movie_loading_message, Snackbar.LENGTH_LONG).show();
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        if (!newText.isEmpty()) {
            asyncSearch(newText);
            getIntent().putExtra(Constants.EXTRA_SEARCH_INPUT, newText);
        }
        return false;
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        if (!query.isEmpty())
            handleIntent(getIntent());
        return false;
    }
}
