package com.damsky.danny.bambatime.util.task;

import android.os.AsyncTask;
import android.support.annotation.NonNull;
import android.util.Log;

import com.damsky.danny.bambatime.data.localdb.model.Movie;
import com.damsky.danny.bambatime.util.Constants;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * This class Asynchronously parses a JSON of search results from the TMDB API.
 *
 * @author Danny Damsky
 */
public final class SearchTask extends AsyncTask<String, Void, List<Movie>> {

    private SearchTaskCallback callback;
    private boolean errorHappened = false;

    public SearchTask(@NonNull SearchTaskCallback callback) {
        this.callback = callback;
    }

    /**
     * @param strings Should only have one parameter, and that parameter should be a JSON String.
     * @return An ArrayList of movies that the JSON String contained.
     */
    @Override
    protected List<Movie> doInBackground(@NonNull String... strings) {
        List<Movie> movies = null;
        try {
            // Get the JSON
            JSONObject object = new JSONObject(strings[0]);

            // Get the array of movie results
            JSONArray results = object.getJSONArray(Constants.SEARCH_RESULTS);

            // Initialize the Movie ArrayList with the same size as the JSONArray
            movies = new ArrayList<>(results.length());

            // Start iterating over each result in the JSONArray and fill the movies ArrayList
            for (int i = 0; i < results.length(); i++) {
                JSONObject obj = results.getJSONObject(i);

                Long id = obj.getLong(Constants.MOVIE_ID);
                boolean adult = obj.getBoolean(Constants.MOVIE_ADULT);
                String title = obj.getString(Constants.MOVIE_TITLE);
                String overview = obj.getString(Constants.MOVIE_OVERVIEW);
                String release_date = obj.getString(Constants.MOVIE_RELEASE_DATE);
                double vote_average = obj.getDouble(Constants.MOVIE_VOTE_AVERAGE);
                double popularity = obj.getDouble(Constants.MOVIE_POPULARITY);
                String poster_path = obj.getString(Constants.MOVIE_POSTER_PATH);
                String backdrop_path = obj.getString(Constants.MOVIE_BACKDROP_PATH);

                Movie movie = new Movie(id, adult, title, overview, release_date, vote_average,
                        popularity, poster_path, backdrop_path);

                movies.add(movie);
            }
        } catch (JSONException e) {
            // This should never happen. If it does, the JSON was incorrect.
            Log.e("ErrorInSearchTask", e.getMessage());
            errorHappened = true;
        }

        return movies;
    }

    @Override
    protected void onPostExecute(List<Movie> simpleMovies) {
        super.onPostExecute(simpleMovies);

        // If errorMessage is not empty that means there was an error during doInBackground().

        if (!errorHappened)
            callback.onSearchSucceeded(simpleMovies);

        callback = null;
    }

    public interface SearchTaskCallback {
        void onSearchSucceeded(List<Movie> movies);
    }
}
