package com.damsky.danny.bambatime.util.task;

import android.os.AsyncTask;
import android.support.annotation.NonNull;
import android.util.Log;

import com.damsky.danny.bambatime.data.localdb.model.Genre;
import com.damsky.danny.bambatime.util.Constants;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * This task is used to query all genres that are available in TMDB.
 *
 * @author Danny Damsky
 */

public final class GenreQueryTask extends AsyncTask<String, Void, List<Genre>> {

    private GenreQueryTaskCallback callback;
    private boolean errorHappened = false;

    public GenreQueryTask(@NonNull GenreQueryTaskCallback callback) {
        this.callback = callback;
    }

    /**
     * @param strings Should be only one string, and it should contain a JSON of genres.
     * @return An ArrayList of genres, or null if the JSON parsing operation failed.
     */

    @Override
    protected List<Genre> doInBackground(String... strings) {
        List<Genre> genres = null;
        try {
            JSONObject object = new JSONObject(strings[0]);
            JSONArray array = object.getJSONArray(Constants.GENRE_ARRAY);
            genres = new ArrayList<>(array.length());

            for (int i = 0; i < array.length(); i++) {
                JSONObject obj = array.getJSONObject(i);
                Long id = obj.getLong(Constants.GENRE_ID);
                String name = obj.getString(Constants.GENRE_NAME);
                genres.add(new Genre(id, name));
            }
        } catch (JSONException e) {
            // This should never happen. If it does, the JSON was incorrect.
            Log.e("ErrorInGenreQueryTask", e.getMessage());
            errorHappened = true;
        }

        return genres;
    }

    @Override
    protected void onPostExecute(List<Genre> genres) {
        super.onPostExecute(genres);
        if (!errorHappened)
            callback.onGenreQuerySucceeded(genres);

        callback = null;
    }

    public interface GenreQueryTaskCallback {
        void onGenreQuerySucceeded(List<Genre> genreList);
    }
}
