package com.damsky.danny.bambatime.ui.widget;

import android.content.Context;
import android.os.Handler;
import android.support.v7.widget.AppCompatTextView;
import android.util.AttributeSet;
import android.util.Log;

/**
 * This View is the same as a regular TextView with one difference:
 * It contains start() and stop() functions which respectively start and stop a 3-dot
 * loading animation in the TextView.
 *
 * @author Danny Damsky
 * @see AppCompatTextView
 */

public final class ProgressText extends AppCompatTextView {

    private static final int UPDATE_TEXT_INTERVAL_IN_MILLIS = 500;

    private Handler handler;
    private Runnable runnable;
    private boolean previouslyStopped = true;

    public ProgressText(Context context) {
        super(context);
    }

    public ProgressText(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public ProgressText(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    /**
     * Starts a 3-dot loading animation if it isn't running.
     */
    public void start() {
        if (previouslyStopped) {
            runnable = () -> {
                try {
                    String text = getText().toString();
                    if (text.endsWith("..."))
                        setText(text.subSequence(0, text.length() - 3));
                    else
                        append(".");
                } catch (Exception e) {
                    Log.e("ErrorInStart", e.getMessage());
                }
                handler.postDelayed(runnable, UPDATE_TEXT_INTERVAL_IN_MILLIS);
            };

            handler = new Handler();
            handler.postDelayed(runnable, UPDATE_TEXT_INTERVAL_IN_MILLIS);
        }
    }

    /**
     * Stops the animation if it's running.
     */
    public void stop() {
        if (!previouslyStopped) {
            if (handler != null)
                handler.removeCallbacks(runnable);
            runnable = null;
            handler = null;

            previouslyStopped = true;
        }
    }
}
