package com.damsky.danny.bambatime.util;

import android.content.Context;
import android.support.annotation.NonNull;

import com.damsky.danny.bambatime.R;

import java.text.NumberFormat;
import java.util.Locale;

/**
 * This class is used for converting elements taken from parsed JSONs into whatever
 * information is necessary.
 *
 * @author Danny Damsky
 */

public final class JSONFormatConverter {

    /**
     * @param context                 The context of an activity or the application.
     * @param nonFormattedReleaseDate The release date as it was taken from a JSON
     *                                (Format: YYYY-MM-DD)
     * @return A formatted release date that looks like this: January 01, 2018 (Varies by locale)
     */
    public static String getFormattedReleaseDate(@NonNull Context context, @NonNull String nonFormattedReleaseDate) {
        try {
            String[] dateNumbers = nonFormattedReleaseDate.split("-");

            int month = Integer.parseInt(dateNumbers[1]);
            String monthName = context.getResources().getStringArray(R.array.months)[month - 1];
            return context.getResources().getString(R.string.formatted_date, monthName, dateNumbers[2], dateNumbers[0]);
        } catch (Exception e) {
            return nonFormattedReleaseDate;
        }
    }

    /**
     * @param context The context of an activity or application.
     * @param runtime The runtime as it was taken from a JSON. (In minutes)
     *
     * @return A formatted runtime String that looks like this: 2h, 5m
     */
    public static String getFormattedRuntime(@NonNull Context context, int runtime) {
        int hours = runtime / 60;
        int minutes = runtime - (hours * 60);

        return context.getResources().getString(R.string.formatted_runtime, hours, minutes);
    }

    /**
     * @param vote_average The movie's rating as it was taken from a JSON.
     *                     (Format: A double value ranging from 0 to 10)
     *
     * @return The vote average in percents (An integer value from 0 to 100)
     */
    public static int getVoteAverageValueInPercent(double vote_average) {
        return (int) (vote_average * 10);
    }

    /**
     * @param moneyValue An integer that represents an amount of money in dollars. (For example: 100)
     *
     * @return A formatted representation of moneyValue, according to US Locale. (For example: $100.00)
     */
    public static String getFormattedMoneyString(int moneyValue) {
        Locale locale = new Locale("en", "US");
        NumberFormat numberFormat = NumberFormat.getCurrencyInstance(locale);
        return numberFormat.format(moneyValue);
    }

    /**
     * @param langCode A language code, usually the first two letters from the language's name.
     *                 (For example: "en" for English)
     *
     * @return The language's full name.
     */
    public static String getLanguage(@NonNull String langCode) {
        Locale locale = new Locale(langCode);
        return locale.getDisplayLanguage();
    }

    private JSONFormatConverter() {
    }
}
