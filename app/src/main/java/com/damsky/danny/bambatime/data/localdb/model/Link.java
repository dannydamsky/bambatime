package com.damsky.danny.bambatime.data.localdb.model;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Generated;
import org.greenrobot.greendao.annotation.Id;
import org.greenrobot.greendao.annotation.Keep;
import org.greenrobot.greendao.annotation.NotNull;

@Entity(nameInDb = "LINK")
public class Link {
    @Id(autoincrement = true)
    private Long id;

    @NotNull
    private Long movieId;

    @NotNull
    private Long genreId;

    @Generated(hash = 1422673044)
    public Link(Long id, @NotNull Long movieId, @NotNull Long genreId) {
        this.id = id;
        this.movieId = movieId;
        this.genreId = genreId;
    }

    @Keep
    public Link(@NotNull Long movieId, @NotNull Long genreId) {
        this.id = null;
        this.movieId = movieId;
        this.genreId = genreId;
    }

    @Generated(hash = 225969300)
    public Link() {
    }

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getMovieId() {
        return this.movieId;
    }

    public void setMovieId(Long movieId) {
        this.movieId = movieId;
    }

    public Long getGenreId() {
        return this.genreId;
    }

    public void setGenreId(Long genreId) {
        this.genreId = genreId;
    }
}
