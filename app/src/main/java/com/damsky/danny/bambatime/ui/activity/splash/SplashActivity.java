package com.damsky.danny.bambatime.ui.activity.splash;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.damsky.danny.bambatime.App;
import com.damsky.danny.bambatime.R;
import com.damsky.danny.bambatime.ui.activity.main.MainActivity;
import com.damsky.danny.bambatime.ui.dialog.alertdialog.MessageDialog;
import com.damsky.danny.bambatime.ui.widget.ProgressText;
import com.damsky.danny.bambatime.util.task.GenreQueryTask;
import com.damsky.danny.bambatime.util.task.NetworkTask;

import java.util.Calendar;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * This is the splash screen of the application.
 * It's the first thing that runs and it's in charge of loading the Discover query JSON for the
 * MainActivity to parse.
 * <p>
 * On the first run this activity will also present a "Welcome" dialog to the user.
 *
 * @author Danny Damsky
 * @see MessageDialog
 * @see NetworkTask
 */

public final class SplashActivity extends AppCompatActivity
        implements MessageDialog.OnMessageDialogDismissListener {

    private App appReference;

    @BindView(R.id.progressBar)
    ProgressBar progressBar;
    @BindView(R.id.loadingText)
    ProgressText loadingText;
    private NetworkTask discoverNetworkTask;
    private NetworkTask genreNetworkTask;
    private GenreQueryTask genreJsonTask;

    private String thisWebSource;
    private boolean discoverTaskFinished = false;
    private boolean genreTaskFinished = false;

    private boolean success = false;

    private void cancelTask(AsyncTask task) {
        if (task != null)
            task.cancel(true);
    }

    private void startMain() {
        Intent startMain = new Intent(SplashActivity.this, MainActivity.class);
        startActivity(startMain);
        finish();
    }

    // To be used for when an error occurred but it wasn't a deal-breaker.
    private void startMainWithError(boolean aTaskFinished) {
        if (aTaskFinished) {
            if (appReference.getPreferencesHelper().isFirstRun())
                MessageDialog.show(getSupportFragmentManager(), R.string.error_network,
                        R.string.error_network_message,
                        android.R.string.ok);
            else {
                Toast.makeText(getApplicationContext(), R.string.error_network_non_critical, Toast.LENGTH_LONG).show();
                startMain();
            }
        }
    }

    /* Download a JSON of Movie results from a URL and start MainActivity if everything went well
       and the loadGenresJson() finished. */
    private void loadDiscoverJson() {
        if (appReference.getPreferencesHelper().updateRequired(getString(R.string.language_code))) {
            String discoverUrl = getString(R.string.tmdb_discover_query,
                    Calendar.getInstance().get(Calendar.YEAR));

            discoverNetworkTask = new NetworkTask(new NetworkTask.NetworkTaskCallback() {
                @Override
                public void onDownloadStarted() {
                    progressBar.setVisibility(View.VISIBLE);
                    loadingText.setText(R.string.loading_movies);
                    loadingText.start();
                    loadGenresJson();
                }

                @Override
                public void onDownloadSucceeded(String webSource) {
                    discoverTaskFinished = true;
                    thisWebSource = webSource;

                    /* If the genre AsyncTask finished then this function should be the one
                       to start the MainActivity.
                       If the genre AsyncTask hasn't finished yet then it will be the one to start
                       MainActivity when it finishes. */

                    if (genreTaskFinished) {
                        progressBar.setVisibility(View.GONE);

                        appReference.getPreferencesHelper().setDiscoverCache(webSource);

                        if (appReference.getPreferencesHelper().isFirstRun()) {
                            appReference.getPreferencesHelper().setIsNotFirstRun();
                            MessageDialog.show(getSupportFragmentManager(), R.string.welcome,
                                    R.string.welcome_message,
                                    R.string.awesome);
                            success = true;
                        } else
                            startMain();
                    }
                }

                @Override
                public void onDownloadFailed() {
                    startMainWithError(genreTaskFinished);
                }
            });
            discoverNetworkTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, discoverUrl);
        } else
            startMain();
    }

    /* Download a JSON of Genres from a URL and start MainActivity if everything went well and the
       loadDiscoverJson() function finished. */
    private void loadGenresJson() {
        genreNetworkTask = new NetworkTask(new NetworkTask.NetworkTaskCallback() {
            @Override
            public void onDownloadSucceeded(String webSource) {
                genreJsonTask = new GenreQueryTask(genreList -> {

                    appReference.getDbHelper().insertGenres(genreList);

                    genreTaskFinished = true;

                    /* If the discover AsyncTask finished then this function should be the one
                       to start the MainActivity.
                       If the discover AsyncTask hasn't finished yet then it will be the one to start
                       MainActivity when it finishes. */

                    if (discoverTaskFinished) {
                        progressBar.setVisibility(View.GONE);

                        appReference.getPreferencesHelper().setDiscoverCache(thisWebSource);

                        if (appReference.getPreferencesHelper().isFirstRun()) {
                            appReference.getPreferencesHelper().setIsNotFirstRun();
                            MessageDialog.show(getSupportFragmentManager(), R.string.welcome,
                                    R.string.welcome_message,
                                    R.string.awesome);
                            success = true;
                        } else
                            startMain();
                    }
                });

                genreJsonTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, webSource);
            }

            @Override
            public void onDownloadFailed() {
                startMainWithError(discoverTaskFinished);
            }
        });
        genreNetworkTask.execute(getString(R.string.tmdb_genres_query));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        ButterKnife.bind(this);
        appReference = (App) getApplication();
    }

    @Override
    protected void onResume() {
        super.onResume();
        loadDiscoverJson();
    }

    @Override
    protected void onPause() {
        cancelTask(discoverNetworkTask);
        cancelTask(genreNetworkTask);
        cancelTask(genreJsonTask);
        loadingText.stop();
        super.onPause();
    }

    @Override
    public void onBackPressed() {
    }

    @Override
    public void onMessageDialogDismissed() {
        if (success) {
            success = false;
            startMain();
        } else
            finish();
    }
}
