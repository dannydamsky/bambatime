package com.damsky.danny.bambatime.ui.dialog.moviedetails;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.TextInputEditText;
import android.widget.DatePicker;
import android.widget.Spinner;

import com.damsky.danny.bambatime.data.localdb.model.Movie;
import com.damsky.danny.bambatime.ui.widget.FiveStarRatingBar;
import com.damsky.danny.bambatime.ui.widget.TimePickerCompat;
import com.damsky.danny.bambatime.util.Constants;

import java.util.Objects;

/**
 * This class is used mostly for MovieDetailsDialog.
 * It holds a Bundle object which it fills up (via one of its constructors) with movie info that
 * is relevant to the dialog.
 * <p>
 * It also contains features for getting a Movie object using the info stored in the bundle.
 *
 * @author Danny Damsky
 * @see MovieDetailsDialog
 * @see Movie
 * @see Constants
 * @see Bundle
 */

public class BundleHolder {

    private final Bundle bundle;

    /**
     * This accepts all the views from MovieDetailsDialog, the information that they hold is
     * stored inside the bundle.
     *
     * @param movie_title        An EditText with the movie's title.
     * @param movie_tagline      An EditText with the movie's tagline.
     * @param movie_overview     An EditText with the movie's overview.
     * @param movie_date         A DatePicker object with the movie's release date.
     * @param movie_rating       A FiveStarRatingBar object with the movie's rating from 0-10.
     * @param movie_budget       A number-only EditText with the movie's budget.
     * @param movie_revenue      A number-only EditText with the movie's revenue.
     * @param movie_runtime      A TimePickerCompat object with the movie's runtime. (hours and minutes)
     * @param movie_status       A spinner with an array of possible movie statuses.
     * @param poster_path        The movie's original poster web path (TMDB)
     * @param backdrop_path      The movie's original backdrop web path (TMDB)
     * @param poster_path_user   The movie's poster path (user input)
     * @param backdrop_path_user The movie's backdrop path (user input)
     */
    public BundleHolder(@NonNull TextInputEditText movie_title, @NonNull TextInputEditText movie_tagline,
                        @NonNull TextInputEditText movie_overview, @NonNull DatePicker movie_date,
                        @NonNull FiveStarRatingBar movie_rating, @NonNull TextInputEditText movie_budget,
                        @NonNull TextInputEditText movie_revenue, @NonNull TimePickerCompat movie_runtime,
                        @NonNull Spinner movie_status, String poster_path, String backdrop_path,
                        String poster_path_user, String backdrop_path_user) {
        bundle = new Bundle();
        bundle.putString(Constants.MOVIE_TITLE, movie_title.getText().toString());
        bundle.putString(Constants.MOVIE_TAGLINE, movie_tagline.getText().toString());
        bundle.putString(Constants.MOVIE_OVERVIEW, movie_overview.getText().toString());
        bundle.putInt(Constants.MOVIE_RELEASE_DATE_YEAR, movie_date.getYear());
        bundle.putInt(Constants.MOVIE_RELEASE_DATE_MONTH, movie_date.getMonth());
        bundle.putInt(Constants.MOVIE_RELEASE_DATE_DAY, movie_date.getDayOfMonth());
        bundle.putDouble(Constants.MOVIE_VOTE_AVERAGE, (double) movie_rating.getRating());
        bundle.putString(Constants.MOVIE_BUDGET, movie_budget.getText().toString());
        bundle.putString(Constants.MOVIE_REVENUE, movie_revenue.getText().toString());

        bundle.putInt(Constants.MOVIE_RUNTIME_HOUR, movie_runtime.getHour());
        bundle.putInt(Constants.MOVIE_RUNTIME_MINUTE, movie_runtime.getMinute());

        bundle.putString(Constants.MOVIE_STATUS, (String) movie_status.getSelectedItem());

        bundle.putString(Constants.MOVIE_POSTER_PATH, poster_path);
        bundle.putString(Constants.MOVIE_BACKDROP_PATH, backdrop_path);
        bundle.putString(Constants.MOVIE_POSTER_PATH_USER, poster_path_user);
        bundle.putString(Constants.MOVIE_BACKDROP_PATH_USER, backdrop_path_user);
    }

    /**
     * This accepts a movie object and stores its information inside a bundle.
     *
     * @param movie A movie object.
     */
    public BundleHolder(@NonNull Movie movie) {
        bundle = new Bundle();
        bundle.putString(Constants.MOVIE_TITLE, movie.getTitle());
        bundle.putString(Constants.MOVIE_TAGLINE, movie.getTagline());
        bundle.putString(Constants.MOVIE_OVERVIEW, movie.getOverview());

        if (movie.getRelease_date() != null) {
            String[] dateNumbers = movie.getRelease_date().split("-");
            bundle.putInt(Constants.MOVIE_RELEASE_DATE_YEAR, Integer.parseInt(dateNumbers[0]));
            bundle.putInt(Constants.MOVIE_RELEASE_DATE_MONTH, Integer.parseInt(dateNumbers[1]) - 1);
            bundle.putInt(Constants.MOVIE_RELEASE_DATE_DAY, Integer.parseInt(dateNumbers[2]));
        }

        bundle.putDouble(Constants.MOVIE_VOTE_AVERAGE, movie.getVote_average());
        bundle.putString(Constants.MOVIE_BUDGET, movie.getBudget() + "");
        bundle.putString(Constants.MOVIE_REVENUE, movie.getRevenue() + "");

        int movie_runtime_hour = movie.getRuntime() / 60;
        int movie_runtime_minute = movie.getRuntime() - (movie_runtime_hour * 60);
        bundle.putInt(Constants.MOVIE_RUNTIME_HOUR, movie_runtime_hour);
        bundle.putInt(Constants.MOVIE_RUNTIME_MINUTE, movie_runtime_minute);

        bundle.putString(Constants.MOVIE_STATUS, movie.getStatus());

        bundle.putString(Constants.MOVIE_POSTER_PATH, movie.getPoster_path());
        bundle.putString(Constants.MOVIE_BACKDROP_PATH, movie.getBackdrop_path());
        bundle.putString(Constants.MOVIE_POSTER_PATH_USER, movie.getPoster_path_user());
        bundle.putString(Constants.MOVIE_BACKDROP_PATH_USER, movie.getBackdrop_path_user());
    }

    /**
     * This constructor simple holds a bundle for future use with updateMovie() and getNewMovie()
     * functions.
     *
     * @param bundle The bundle to hold, must contain values with keys from .util.Constants.
     */
    public BundleHolder(@NonNull Bundle bundle) {
        this.bundle = bundle;
    }

    /**
     * @return The bundle object.
     */
    public final Bundle getBundle() {
        return bundle;
    }

    /**
     * This function updates the movie that is given as a parameter with the information that is
     * stored inside the bundle object.
     *
     * @param movie The movie to update.
     */
    public final void updateMovie(@NonNull Movie movie) {
        String title = bundle.getString(Constants.MOVIE_TITLE);
        String tagline = bundle.getString(Constants.MOVIE_TAGLINE);
        String overview = bundle.getString(Constants.MOVIE_OVERVIEW);
        String release_date = bundle.getInt(Constants.MOVIE_RELEASE_DATE_YEAR) + "-" +
                (bundle.getInt(Constants.MOVIE_RELEASE_DATE_MONTH) + 1) + "-" +
                bundle.getInt(Constants.MOVIE_RELEASE_DATE_DAY);
        double vote_average = bundle.getDouble(Constants.MOVIE_VOTE_AVERAGE);
        int budget = getIntFromString(Objects.requireNonNull(bundle.getString(Constants.MOVIE_BUDGET)));
        int revenue = getIntFromString(Objects.requireNonNull(bundle.getString(Constants.MOVIE_REVENUE)));

        int runtime_hour = bundle.getInt(Constants.MOVIE_RUNTIME_HOUR);
        int runtime_minute = bundle.getInt(Constants.MOVIE_RUNTIME_MINUTE);
        int runtime = (runtime_hour * 60) + runtime_minute;

        String status = bundle.getString(Constants.MOVIE_STATUS);
        String poster_path_user = bundle.getString(Constants.MOVIE_POSTER_PATH_USER);
        String backdrop_path_user = bundle.getString(Constants.MOVIE_BACKDROP_PATH_USER);

        movie.setTitle(title);
        movie.setTagline(tagline);
        movie.setOverview(overview);
        movie.setRelease_date(release_date);
        movie.setVote_average(vote_average);
        movie.setBudget(budget);
        movie.setRevenue(revenue);

        if (runtime > 0)
            movie.setRuntime(runtime);

        movie.setStatus(status);
        movie.setPoster_path_user(poster_path_user);
        movie.setBackdrop_path_user(backdrop_path_user);

        movie.setModifiable(true);
    }

    /**
     * @return A new movie object constructed purely from the information stored in the bundle object.
     */
    public final Movie getNewMovie() {
        String title = bundle.getString(Constants.MOVIE_TITLE);
        String tagline = bundle.getString(Constants.MOVIE_TAGLINE);
        String overview = bundle.getString(Constants.MOVIE_OVERVIEW);
        String release_date = bundle.getInt(Constants.MOVIE_RELEASE_DATE_YEAR) + "-" +
                (bundle.getInt(Constants.MOVIE_RELEASE_DATE_MONTH) + 1) + "-" +
                bundle.getInt(Constants.MOVIE_RELEASE_DATE_DAY);
        double vote_average = bundle.getDouble(Constants.MOVIE_VOTE_AVERAGE);

        int budget = getIntFromString(Objects.requireNonNull(bundle.getString(Constants.MOVIE_BUDGET)));
        int revenue = getIntFromString(Objects.requireNonNull(bundle.getString(Constants.MOVIE_REVENUE)));

        int runtime_hour = bundle.getInt(Constants.MOVIE_RUNTIME_HOUR);
        int runtime_minute = bundle.getInt(Constants.MOVIE_RUNTIME_MINUTE);
        int runtime = (runtime_hour * 60) + runtime_minute;

        String status = bundle.getString(Constants.MOVIE_STATUS);
        String poster_path_user = bundle.getString(Constants.MOVIE_POSTER_PATH_USER);
        String backdrop_path_user = bundle.getString(Constants.MOVIE_BACKDROP_PATH_USER);

        return new Movie(null, null, false, title, tagline, overview, release_date,
                null, null, -1, vote_average, 100.0, budget, revenue, runtime, status, null, null, null,
                null, true, false, false, poster_path_user, backdrop_path_user);
    }

    private int getIntFromString(String s) {
        if (!s.isEmpty())
            return Integer.parseInt(s);
        return -1;
    }
}
