package com.damsky.danny.bambatime.ui.activity.main.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.damsky.danny.bambatime.GlideApp;
import com.damsky.danny.bambatime.R;
import com.damsky.danny.bambatime.data.localdb.model.Movie;
import com.damsky.danny.bambatime.ui.widget.PrettyProgressBar;
import com.damsky.danny.bambatime.util.JSONFormatConverter;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * An adapter for a RecyclerView of Movies.
 * NOTE: The layout of this adapter is different for different screen sizes/orientations.
 *
 * @author Danny Damsky
 */

public final class MainRecycleAdapter extends RecyclerView.Adapter<MainRecycleAdapter.ViewHolder> {

    private final OnCardClickListener listener;
    private final List<Movie> movies;
    private final boolean shouldEnableBigLayout;

    /**
     * @param listener              A listener that implements callbacks sent back from this adapter.
     *                              It is recommended to implement the listener in the activity and
     *                              pass the activity's context as the parameter here.
     * @param movies                A list of movies to load in the adapter.
     * @param shouldEnableBigLayout A boolean to determine whether or not the adapter should
     *                              prepare the layout to be used with a big screen or a screen
     *                              in landscape mode.
     */
    public MainRecycleAdapter(@NonNull OnCardClickListener listener, List<Movie> movies, boolean shouldEnableBigLayout) {
        this.listener = listener;
        this.movies = movies;
        this.shouldEnableBigLayout = shouldEnableBigLayout;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.recyclerview_movie_item, parent, false);

        return new ViewHolder(itemView);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        Movie thisMovie = movies.get(position);
        holder.movieTitle.setText(thisMovie.getTitle());
        holder.movieDate.setText(JSONFormatConverter.getFormattedReleaseDate(holder.card.getContext(), thisMovie.getRelease_date()));

        Context context = holder.movieImage.getContext();
        if (shouldEnableBigLayout) {
            Bitmap bitmap = BitmapFactory.decodeFile(thisMovie.getPoster_path_user());
            if (bitmap != null)
                GlideApp.with(context)
                        .load(bitmap)
                        .placeholder(R.drawable.placeholder_no_image_available_poster)
                        .into(holder.movieImage);
            else
                GlideApp.with(context)
                        .load(context.getString(R.string.tmdb_image_url, thisMovie.getPoster_path()))
                        .placeholder(R.drawable.placeholder_no_image_available_poster)
                        .into(holder.movieImage);

            holder.movieOverview.setText(thisMovie.getOverview());
        } else {
            Bitmap bitmap = BitmapFactory.decodeFile(thisMovie.getBackdrop_path_user());
            if (bitmap != null)
                GlideApp.with(context)
                        .load(bitmap)
                        .placeholder(R.drawable.placeholder_no_image_available_backdrop)
                        .into(holder.movieImage);
            else
                GlideApp.with(context)
                        .load(context.getString(R.string.tmdb_image_url, thisMovie.getBackdrop_path()))
                        .placeholder(R.drawable.placeholder_no_image_available_backdrop)
                        .into(holder.movieImage);
        }

        int percent = JSONFormatConverter.getVoteAverageValueInPercent(thisMovie.getVote_average());

        holder.movieRating.setProgress(percent);

        holder.card.setOnClickListener(v -> listener.onCardClicked(position));
        holder.card.setOnLongClickListener(v -> {
            listener.onCardLongClicked(position);
            return true;
        });

        if (thisMovie.getIs_seen())
            holder.card.setAlpha(0.75f);
        else
            holder.card.setAlpha(1f);
    }

    @Override
    public int getItemCount() {
        return movies.size();
    }

    static class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.card)
        CardView card;
        @BindView(R.id.movieImage)
        ImageView movieImage;
        @BindView(R.id.movieRating)
        PrettyProgressBar movieRating;
        @BindView(R.id.movieTitle)
        TextView movieTitle;
        @BindView(R.id.movieDate)
        TextView movieDate;
        @BindView(R.id.movieOverview)
        TextView movieOverview;

        ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    public interface OnCardClickListener {
        void onCardClicked(int position);

        default void onCardLongClicked(int position) {
        }
    }
}
