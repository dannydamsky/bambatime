package com.damsky.danny.bambatime.ui.activity.prefs;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceFragment;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.app.AppCompatDelegate;

import com.damsky.danny.bambatime.App;
import com.damsky.danny.bambatime.R;
import com.damsky.danny.bambatime.data.prefs.PreferencesHelper;
import com.damsky.danny.bambatime.util.Constants;

/**
 * This is the "Settings" activity.
 * Its use is changing the default SharedPreferences in the application. (Such as: Night Mode true/false)
 *
 * @author Danny Damsky
 * @see SharedPreferences
 * @see PreferencesHelper
 */

public final class PreferencesActivity extends AppCompatActivity implements
        SharedPreferences.OnSharedPreferenceChangeListener {

    private boolean adultSettingChanged = false;
    private boolean isAdult = false;
    private App appReference;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        appReference = (App) getApplication();

        getFragmentManager().beginTransaction()
                .replace(android.R.id.content, new PreferencesFragment()).commit();

        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(this);
        preferences.registerOnSharedPreferenceChangeListener(this);

        adultSettingChanged = getIntent().getBooleanExtra(Constants.EXTRA_ADULT_SETTING_CHANGED, false);
        isAdult = preferences.getBoolean(PreferencesHelper.DEFAULT_PREFERENCE_IS_ADULT, false);

        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public boolean onSupportNavigateUp() {
        finish();
        return super.onSupportNavigateUp();
    }

    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
        switch (key) {
            case PreferencesHelper.DEFAULT_PREFERENCE_IS_ADULT:
                adultSettingChanged = true;
                isAdult = sharedPreferences.getBoolean(key, false);
                break;
            case PreferencesHelper.DEFAULT_PREFERENCE_NIGHT_MODE:
                if (sharedPreferences.getBoolean(key, false))
                    AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES);
                else
                    AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);
                getIntent().putExtra(Constants.EXTRA_NIGHT_MODE_CHANGED,
                        !getIntent().getBooleanExtra(Constants.EXTRA_NIGHT_MODE_CHANGED, false));
                this.recreate();
                break;
        }
    }

    @Override
    protected void onPause() {
        getIntent().putExtra(Constants.EXTRA_ADULT_SETTING_CHANGED, adultSettingChanged);
        super.onPause();
    }

    @Override
    public void finish() {
        if (adultSettingChanged) {
            appReference.getPreferencesHelper().clearCacheExceptForDiscoverCache();
            getIntent().putExtra(Constants.EXTRA_ADULT_SETTING_CHANGED, true);

            if (!isAdult)
                appReference.getDbHelper().deleteAllAdultContent();
        }

        setResult(RESULT_OK, getIntent());
        super.finish();
    }

    public static class PreferencesFragment extends PreferenceFragment {
        @Override
        public void onCreate(@Nullable Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            addPreferencesFromResource(R.xml.preferences);
        }
    }
}
