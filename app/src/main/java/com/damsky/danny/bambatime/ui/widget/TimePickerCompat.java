package com.damsky.danny.bambatime.ui.widget;

import android.content.Context;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.TextInputEditText;
import android.util.AttributeSet;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TimePicker;

import com.damsky.danny.bambatime.R;
import com.damsky.danny.bambatime.util.Constants;

/**
 * Because TimePicker's setHour()/setMinute() functions aren't available in APIs below 23 this class
 * makes sure to only show TimePicker in API 23 and above.
 * <p>
 * If the API is below 23 the TimePicker is replaced with 2 horizontal EditTexts for hours and minutes.
 *
 * @author Danny Damsky
 * @see TimePicker
 * @see TextInputEditText
 */

public class TimePickerCompat extends ConstraintLayout {

    private final TimePicker timePicker;

    private final TextInputEditText hours;
    private final TextInputEditText minutes;

    {
        LayoutInflater layoutInflater = LayoutInflater.from(getContext());
        View thisView = layoutInflater.inflate(R.layout.time_picker_compat, this, true);

        if (Constants.IS_MARSHMALLOW_OR_ABOVE) {
            timePicker = thisView.findViewById(R.id.time_picker);
            timePicker.setIs24HourView(true);
            timePicker.setHour(0);
            timePicker.setMinute(0);

            hours = null;
            minutes = null;
        } else {
            hours = thisView.findViewById(R.id.hours);
            hours.setOnKeyListener(new OnKeyListener() {
                @Override
                public boolean onKey(View v, int keyCode, KeyEvent event) {
                    String hours_str = hours.getText().toString();
                    if (!hours_str.isEmpty() && Integer.parseInt(hours_str) > 23) {
                        hours.setText("23");
                        hours.setSelection(2);
                        return true;
                    }

                    return false;
                }
            });

            minutes = thisView.findViewById(R.id.minutes);

            minutes.setOnKeyListener((v, keyCode, event) -> {
                String minutes_str = minutes.getText().toString();
                if (!minutes_str.isEmpty() && Integer.parseInt(minutes_str) > 59) {
                    minutes.setText("59");
                    minutes.setSelection(2);
                    return true;
                }

                return false;

            });

            timePicker = null;
        }
    }

    public TimePickerCompat(Context context) {
        super(context);
    }

    public TimePickerCompat(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public TimePickerCompat(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    /**
     * Sets the currently selected hour using 24-hour time.
     *
     * @param hour the hour to set, in the range (0-23)
     */
    public final void setHour(int hour) {
        if (Constants.IS_MARSHMALLOW_OR_ABOVE)
            timePicker.setHour(hour);
        else
            hours.setText(hour + "");
    }

    /**
     * Sets the currently selected minute..
     *
     * @param minute the minute to set, in the range (0-59)
     */
    public final void setMinute(int minute) {
        if (Constants.IS_MARSHMALLOW_OR_ABOVE)
            timePicker.setMinute(minute);
        else
            minutes.setText(minute + "");
    }

    /**
     * Returns the currently selected hour using 24-hour time.
     *
     * @param hour the currently selected hour, in the range (0-23)
     */
    public final int getHour() {
        if (Constants.IS_MARSHMALLOW_OR_ABOVE)
            return timePicker.getHour();
        else {
            String hour = hours.getText().toString();
            if (hour.isEmpty())
                return 0;

            return Integer.parseInt(hour);
        }
    }

    /**
     * Returns the currently selected minute using 24-hour time.
     *
     * @param minute the currently selected minute, in the range (0-59)
     */
    public final int getMinute() {
        if (Constants.IS_MARSHMALLOW_OR_ABOVE)
            return timePicker.getMinute();
        else {
            String minute = minutes.getText().toString();
            if (minute.isEmpty())
                return 0;

            return Integer.parseInt(minute);
        }
    }
}
