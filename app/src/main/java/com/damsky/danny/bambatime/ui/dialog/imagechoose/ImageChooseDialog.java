package com.damsky.danny.bambatime.ui.dialog.imagechoose;

import android.content.Context;
import android.content.DialogInterface;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.damsky.danny.bambatime.R;
import com.damsky.danny.bambatime.util.Constants;

import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

/**
 * This dialog is used for selecting between picking and image from gallery and taking a photo.
 * The dialog provides callbacks to its attachment for each situation.
 *
 * @author Danny Damsky
 * @see DialogFragment
 */

public final class ImageChooseDialog extends DialogFragment {
    private static final String TAG = "image_choose_dialog_fragment";

    /**
     * @param fragmentManager Your activity's FragmentManager. (If you're running this function from
     *                        an activity that extends FragmentActivity then you should run
     *                        getFragmentManager() for this parameter)
     */
    public static void show(@NonNull FragmentManager fragmentManager) {
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        Fragment previousDialog = fragmentManager.findFragmentByTag(TAG);

        // Remove any running fragments with the same tag.

        if (previousDialog != null)
            fragmentTransaction.remove(previousDialog);

        fragmentTransaction.addToBackStack(null);

        ImageChooseDialog newFragmentDialog = new ImageChooseDialog();
        newFragmentDialog.show(fragmentTransaction, TAG);
    }

    private OnImageChooseDialogDismissListener callback;
    private boolean buttonPressed = false;
    private Unbinder unbinder;

    /**
     * @param context must implement ImageChooseDialog.OnImageChooseDialogDismissListener
     */
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        callback = (OnImageChooseDialogDismissListener) context;
    }

    @Override
    public void onStart() {
        super.onStart();

        // Set the dialog to maximum size.

        getDialog().getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.MATCH_PARENT);

        // Set the dialog's background to R.color.semiTransparent.

        if (Constants.IS_MARSHMALLOW_OR_ABOVE)
            getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.semiTransparent, null)));
        else
            getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.semiTransparent)));
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View dialog = inflater.inflate(R.layout.dialog_image_choose, container, false);
        unbinder = ButterKnife.bind(this, dialog);
        return dialog;
    }

    @OnClick(R.id.galleryFab)
    public void onGalleryFabClicked() {
        buttonPressed = true;
        dismiss();
        callback.onImageChooseDialogGalleryOption();
    }

    @OnClick(R.id.cameraFab)
    public void onCameraFabClicked() {
        buttonPressed = true;
        dismiss();
        callback.onImageChooseDialogCameraOption();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public void onDismiss(DialogInterface dialog) {
        super.onDismiss(dialog);
        if (buttonPressed)
            buttonPressed = false;
        else
            callback.onImageChooseDialogCancelled();
    }

    public interface OnImageChooseDialogDismissListener {
        void onImageChooseDialogCameraOption();

        void onImageChooseDialogCancelled();

        void onImageChooseDialogGalleryOption();
    }
}
