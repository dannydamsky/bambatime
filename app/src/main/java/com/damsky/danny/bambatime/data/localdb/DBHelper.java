package com.damsky.danny.bambatime.data.localdb;

import android.content.Context;
import android.support.annotation.NonNull;

import com.damsky.danny.bambatime.data.localdb.model.DaoMaster;
import com.damsky.danny.bambatime.data.localdb.model.DaoSession;
import com.damsky.danny.bambatime.data.localdb.model.Genre;
import com.damsky.danny.bambatime.data.localdb.model.GenreDao;
import com.damsky.danny.bambatime.data.localdb.model.Link;
import com.damsky.danny.bambatime.data.localdb.model.LinkDao;
import com.damsky.danny.bambatime.data.localdb.model.Movie;
import com.damsky.danny.bambatime.data.localdb.model.MovieDao;

import java.util.List;

/**
 * This is a helper class for dealing with the database throughout the application.
 *
 * @author Danny Damsky
 */

public final class DBHelper {
    private static final String DB_NAME = "movies.db";

    private final DaoMaster.DevOpenHelper helper;

    /**
     * @param context The application's context is recommended, but any context will do.
     */

    public DBHelper(@NonNull Context context) {
        helper = new DaoMaster.DevOpenHelper(context, DB_NAME);
    }

    private DaoSession getReadableDb() {
        return new DaoMaster(helper.getReadableDb()).newSession();
    }

    private DaoSession getWritableDb() {
        return new DaoMaster(helper.getWritableDb()).newSession();
    }

    /**
     * @return A list of movies that were added by the user.
     */

    public List<Movie> getUserAddedMovies() {
        DaoSession daoSession = getReadableDb();
        return daoSession.getMovieDao().queryBuilder()
                .where(MovieDao.Properties.TmdbId.isNull()).list();
    }

    /**
     * @return A list of movies that were bookmarked by the user.
     */

    public List<Movie> getBookmarkedMovies() {
        DaoSession daoSession = getReadableDb();
        return daoSession.getMovieDao().queryBuilder()
                .where(MovieDao.Properties.Bookmarked.eq(true)).list();
    }

    /**
     * @param tmdbId The ID of the movie in TMDB.
     * @return The movie object.
     */

    public Movie getMovieByTmdbId(@NonNull Long tmdbId) {
        DaoSession daoSession = getReadableDb();
        return daoSession.getMovieDao().queryBuilder()
                .where(MovieDao.Properties.TmdbId.eq(tmdbId)).unique();
    }

    /**
     * @param id The ID of the movie in the local DB.
     * @return The movie object.
     */

    public Movie getMovieById(@NonNull Long id) {
        DaoSession daoSession = getReadableDb();
        return daoSession.getMovieDao().queryBuilder()
                .where(MovieDao.Properties.Id.eq(id)).unique();
    }

    /**
     * @return A list of all the available movie genres.
     */

    public List<Genre> getGenres() {
        DaoSession daoSession = getReadableDb();
        return daoSession.getGenreDao().queryBuilder().list();
    }

    /**
     * @param movie The movie to try to insert or update. If this movie's ID was null before running
     *              the function, afterwards the ID will hold the value given from the DB.
     */
    public void insertMovie(@NonNull Movie movie) {
        DaoSession daoSession = getWritableDb();
        long id = daoSession.insertOrReplace(movie);
        movie.setId(id);
    }

    /**
     * @param movie  The movie that the genres are added to. (Add movie if it doesn't exist yet)
     * @param genres The genres to add to the movie.
     */
    public void insertMovieWithGenres(@NonNull Movie movie, @NonNull List<Genre> genres) {
        DaoSession daoSession = getWritableDb();
        daoSession.runInTx(() -> {
            Movie getMovieFromDb = daoSession.getMovieDao().queryBuilder()
                    .where(MovieDao.Properties.TmdbId.eq(movie.getTmdbId())).unique();
            long id;
            if (getMovieFromDb == null || !getMovieFromDb.getModifiable())
                id = daoSession.insertOrReplace(movie);
            else
                id = getMovieFromDb.getId();

            movie.setId(id);

            for (Genre genre : genres) {
                Genre genreWithId = daoSession.getGenreDao().queryBuilder()
                        .where(GenreDao.Properties.Name.eq(genre.getName())).unique();

                Link testLink = daoSession.getLinkDao().queryBuilder()
                        .where(LinkDao.Properties.MovieId.eq(movie.getId()),
                                LinkDao.Properties.GenreId.eq(genreWithId.getId()))
                        .unique();

                if (testLink == null) {
                    Link link = new Link(movie.getId(), genreWithId.getId());
                    daoSession.insertOrReplace(link);
                    movie.resetGenres();
                    genreWithId.resetMovies();
                }
            }
        });
    }

    /**
     * @param genres A list of genres to add to the DB table.
     */
    public void insertGenres(@NonNull List<Genre> genres) {
        DaoSession daoSession = getWritableDb();
        daoSession.runInTx(() -> {
            for (Genre genre : genres) {
                long id = daoSession.insertOrReplace(genre);
                genre.setId(id);
            }
        });
    }

    /**
     * Updates the movie without any safety checks. Use this only when certain that it won't cause
     * issues in the application.
     *
     * @param movie The movie to update.
     */

    public void updateMovie(@NonNull Movie movie) {
        DaoSession daoSession = getWritableDb();
        daoSession.update(movie);
    }

    /**
     * Deletes the movie from the database as well as deletes all the links that this movie has
     * to any genres in the database.
     *
     * @param movie The movie to delete.
     */

    public void deleteMovie(@NonNull Movie movie) {
        DaoSession daoSession = getWritableDb();
        daoSession.runInTx(() -> {
            List<Link> links = daoSession.getLinkDao().queryBuilder()
                    .where(LinkDao.Properties.MovieId.eq(movie.getId()))
                    .list();

            for (Link link : links) {
                daoSession.getLinkDao().delete(link);
                daoSession.getGenreDao().queryBuilder().where(GenreDao.Properties.Id.eq(link.getGenreId()))
                        .unique().resetMovies();
            }

            daoSession.getMovieDao().delete(movie);
        });
    }

    /**
     * Deletes all adult content from the database.
     */

    public void deleteAllAdultContent() {
        DaoSession daoSession = getWritableDb();

        daoSession.runInTx(() -> {
            List<Movie> listOfAdultMovies = daoSession.getMovieDao().queryBuilder()
                    .where(MovieDao.Properties.Adult.eq(true)).list();

            for (Movie movie : listOfAdultMovies)
                deleteMovie(movie);
        });
    }
}
