package com.damsky.danny.bambatime.ui.dialog.moviedetails;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputEditText;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.DatePicker;
import android.widget.ImageButton;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.damsky.danny.bambatime.GlideApp;
import com.damsky.danny.bambatime.R;
import com.damsky.danny.bambatime.data.localdb.model.Movie;
import com.damsky.danny.bambatime.ui.widget.FiveStarRatingBar;
import com.damsky.danny.bambatime.ui.widget.TimePickerCompat;
import com.damsky.danny.bambatime.util.Constants;

import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

/**
 * This is a dialog that is used for editing movie details.
 *
 * @author Danny Damsky
 * @see DialogFragment
 */

public final class MovieDetailsDialog extends DialogFragment {
    private static final String TAG = "movie_details_dialog_fragment";

    /**
     * @param fragmentManager Your activity's FragmentManager. (If you're running this function from
     *                        an activity that extends FragmentActivity then you should run
     *                        getFragmentManager() for this parameter)
     * @param dialogBundle    A bundle that holds movie properties via constants defined under
     *                        .util.Constants, it is used to fill all the views with movie info,
     *                        you can pass null if you don't have a bundle.
     * @param movie           If you wish to edit a movie or fill the views with the movie's info,
     *                        then you should pass a movie as a parameter here, otherwise pass null.
     * @see BundleHolder
     */
    public static void show(@NonNull FragmentManager fragmentManager, Bundle dialogBundle, Movie movie) {
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        Fragment previousDialog = fragmentManager.findFragmentByTag(TAG);

        // Remove any running fragments with the same tag.

        if (previousDialog != null)
            fragmentTransaction.remove(previousDialog);

        fragmentTransaction.addToBackStack(null);

        MovieDetailsDialog newFragmentDialog = new MovieDetailsDialog();

        /* Pass the bundle to the dialog if it's not null
           If it is null then create a bundle from the movie and pass that to the dialog if the
           movie is not null. */

        if (dialogBundle != null)
            newFragmentDialog.setArguments(dialogBundle);
        else if (movie != null)
            newFragmentDialog.setArguments(new BundleHolder(movie).getBundle());

        newFragmentDialog.show(fragmentTransaction, TAG);
    }

    private OnMovieDetailsDialogDismissListener callback;
    private String poster_path;
    private String backdrop_path;
    private String poster_path_user;
    private String backdrop_path_user;

    @BindView(R.id.title)
    TextView dialog_title;
    @BindView(R.id.movie_rating)
    FiveStarRatingBar movie_rating;
    @BindView(R.id.movie_title)
    TextInputEditText movie_title;
    @BindView(R.id.movie_tagline)
    TextInputEditText movie_tagline;
    @BindView(R.id.movie_overview)
    TextInputEditText movie_overview;
    @BindView(R.id.movie_date)
    DatePicker movie_date;
    @BindView(R.id.movie_budget)
    TextInputEditText movie_budget;
    @BindView(R.id.movie_revenue)
    TextInputEditText movie_revenue;
    @BindView(R.id.movie_runtime)
    TimePickerCompat movie_runtime;
    @BindView(R.id.movie_status)
    Spinner movie_status;
    @BindView(R.id.movie_poster)
    ImageButton movie_poster;
    @BindView(R.id.movie_backdrop)
    ImageButton movie_backdrop;

    private Unbinder unbinder;

    /**
     * @param context must implement MovieDetailsDialog.OnMovieDetailsDialogDismissListener
     */
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        callback = (OnMovieDetailsDialogDismissListener) context;
    }

    @Override
    public void onStart() {
        super.onStart();

        // Set the entire window to maximum size.

        Objects.requireNonNull(getDialog().getWindow())
                .setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View dialog = inflater.inflate(R.layout.dialog_movie_details, container, false);
        unbinder = ButterKnife.bind(this, dialog);
        setupDefaultBehaviour();
        initializeBundle();
        return dialog;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @OnClick(R.id.ok)
    public void onOkButtonPressed() {
        if (!movie_title.getText().toString().isEmpty())
            doOnDialogButtonsPress(Constants.RESULT_CODE_DIALOG_OK);
        else
            Toast.makeText(getActivity().getApplicationContext(),
                    R.string.error_movie_title_empty, Toast.LENGTH_SHORT).show();
    }

    @OnClick(R.id.cancel)
    public void onCancelButtonPressed() {
        dismiss();
    }

    @OnClick(R.id.movie_poster)
    public void onMoviePosterImageButtonPressed() {
        doOnDialogButtonsPress(Constants.RESULT_CODE_POSTER);
    }

    @OnClick(R.id.movie_backdrop)
    public void onMovieBackdropImageButtonPressed() {
        doOnDialogButtonsPress(Constants.RESULT_CODE_BACKDROP);
    }

    private void setupDefaultBehaviour() {
        if (getArguments() != null)
            dialog_title.setText(R.string.edit_movie_details);
    }

    private void doOnDialogButtonsPress(int resultCode) {
        dismiss();
        BundleHolder dialogBundle = new BundleHolder(movie_title, movie_tagline,
                movie_overview, movie_date, movie_rating, movie_budget, movie_revenue,
                movie_runtime, movie_status, poster_path, backdrop_path, poster_path_user,
                backdrop_path_user);

        callback.onMovieDetailsDialogDismissed(resultCode, dialogBundle.getBundle());
    }

    private void initializeBundle() {
        Bundle bundle = getArguments();
        if (bundle != null) {
            movie_title.setText(bundle.getString(Constants.MOVIE_TITLE));
            movie_tagline.setText(bundle.getString(Constants.MOVIE_TAGLINE));
            movie_overview.setText(bundle.getString(Constants.MOVIE_OVERVIEW));
            movie_date.updateDate(
                    bundle.getInt(Constants.MOVIE_RELEASE_DATE_YEAR, 1970),
                    bundle.getInt(Constants.MOVIE_RELEASE_DATE_MONTH, 1),
                    bundle.getInt(Constants.MOVIE_RELEASE_DATE_DAY, 1)
            );

            movie_rating.setRating(bundle.getDouble(Constants.MOVIE_VOTE_AVERAGE));

            String budget_string = bundle.getString(Constants.MOVIE_BUDGET);
            String revenue_string = bundle.getString(Constants.MOVIE_REVENUE);

            if (!Objects.equals(budget_string, "-1"))
                movie_budget.setText(bundle.getString(Constants.MOVIE_BUDGET));

            if (!Objects.equals(revenue_string, "-1"))
                movie_revenue.setText(bundle.getString(Constants.MOVIE_REVENUE));

            movie_runtime.setHour(bundle.getInt(Constants.MOVIE_RUNTIME_HOUR));
            movie_runtime.setMinute(bundle.getInt(Constants.MOVIE_RUNTIME_MINUTE));

            String[] statuses = getResources().getStringArray(R.array.movie_statuses);
            String this_status = bundle.getString(Constants.MOVIE_STATUS);

            boolean flag = true;
            for (int i = 0; i < statuses.length && flag; i++) {
                if (statuses[i].equals(this_status)) {
                    movie_status.setSelection(i);
                    flag = false;
                }
            }

            if (flag)
                movie_status.setSelection(0);

            backdrop_path = bundle.getString(Constants.MOVIE_BACKDROP_PATH);
            poster_path = bundle.getString(Constants.MOVIE_POSTER_PATH);
            backdrop_path_user = bundle.getString(Constants.MOVIE_BACKDROP_PATH_USER);
            poster_path_user = bundle.getString(Constants.MOVIE_POSTER_PATH_USER);

            Bitmap backdrop_path_user_bitmap = BitmapFactory.decodeFile(backdrop_path_user);
            if (backdrop_path_user_bitmap != null)
                GlideApp.with(this)
                        .load(backdrop_path_user_bitmap)
                        .placeholder(R.drawable.placeholder_no_image_available_backdrop)
                        .into(movie_backdrop);
            else
                GlideApp.with(this)
                        .load(getString(R.string.tmdb_image_url, backdrop_path))
                        .placeholder(R.drawable.placeholder_no_image_available_backdrop)
                        .into(movie_backdrop);

            Bitmap poster_path_user_bitmap = BitmapFactory.decodeFile(poster_path_user);
            if (poster_path_user_bitmap != null)
                GlideApp.with(this)
                        .load(poster_path_user_bitmap)
                        .placeholder(R.drawable.placeholder_no_image_available_poster)
                        .into(movie_poster);
            else
                GlideApp.with(this)
                        .load(getString(R.string.tmdb_image_url, poster_path))
                        .placeholder(R.drawable.placeholder_no_image_available_poster)
                        .into(movie_poster);
        } else {
            GlideApp.with(this)
                    .load(R.drawable.placeholder_no_image_available_backdrop)
                    .into(movie_backdrop);

            GlideApp.with(this)
                    .load(R.drawable.placeholder_no_image_available_poster)
                    .into(movie_poster);
        }
    }

    public interface OnMovieDetailsDialogDismissListener {
        void onMovieDetailsDialogDismissed(int resultCode, Bundle bundle);
    }
}
