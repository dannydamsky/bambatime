package com.damsky.danny.bambatime.ui.dialog.alertdialog;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.StringRes;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;

import com.damsky.danny.bambatime.R;
import com.damsky.danny.bambatime.util.Constants;

/**
 * A simple dialog with a single (positive) button.
 *
 * @author Danny Damsky
 * @see AlertDialog
 * @see DialogFragment
 */

public final class MessageDialog extends DialogFragment {
    private static final String TAG = "message_alert_dialog";

    /**
     * @param fragmentManager    Your activity's FragmentManager. (If you're running this function from
     *                           an activity that extends FragmentActivity then you should run
     *                           getFragmentManager() for this parameter)
     * @param titleId            A string resource file for the title of the dialog.
     * @param messageId          A string resource file for the message of the dialog.
     * @param positiveButtonText A string resource file for the positive button of the dialog.
     */

    public static void show(@NonNull FragmentManager fragmentManager, @StringRes int titleId,
                            @StringRes int messageId, @StringRes int positiveButtonText) {
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        Fragment previousDialog = fragmentManager.findFragmentByTag(TAG);

        // Remove any running fragments with the same tag.

        if (previousDialog != null)
            fragmentTransaction.remove(previousDialog);

        fragmentTransaction.addToBackStack(null);

        MessageDialog newFragmentDialog = new MessageDialog();

        // Pass the arguments to the DialogFragment

        Bundle bundle = new Bundle();
        bundle.putInt(Constants.DIALOG_TITLE, titleId);
        bundle.putInt(Constants.DIALOG_MESSAGE, messageId);
        bundle.putInt(Constants.DIALOG_POSITIVE_BUTTON_TEXT, positiveButtonText);
        newFragmentDialog.setArguments(bundle);

        newFragmentDialog.show(fragmentTransaction, MessageDialog.TAG);
    }

    public interface OnMessageDialogDismissListener {
        void onMessageDialogDismissed();
    }

    private OnMessageDialogDismissListener callback;

    /**
     * @param context must implement MessageDialog.OnMessageDialogDismissListener
     */

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        callback = (OnMessageDialogDismissListener) context;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setIcon(R.mipmap.ic_launcher);
        builder.setTitle(getArguments().getInt(Constants.DIALOG_TITLE));
        builder.setMessage(getArguments().getInt(Constants.DIALOG_MESSAGE));

        // Setup the callback.

        builder.setPositiveButton(getArguments().getInt(Constants.DIALOG_POSITIVE_BUTTON_TEXT), (dialog, which) ->
                callback.onMessageDialogDismissed());

        builder.setCancelable(false);
        return builder.create();
    }
}
