[![Get it on Google Play](https://play.google.com/intl/en_gb/badges/images/generic/en_badge_web_generic.png)](https://play.google.com/store/apps/details?id=com.damsky.danny.bambatime)

### What is this repository for? ###

The only application you'll ever need to store your personal movie information! In BambaTime, you can discover new movies, search for movies, add your own movies and add movies to bookmarks. 

In addition, BambaTime provides you with a beautiful UI with light and dark themes that is very easy and fast to navigate in.

The application supports Android Lollipop and above.

Libre Music is written in Java 8, uses GreenDAO 3 ORM for the Database,
Glide for image loading and Butterknife.

This repository is for those who want an easy, free solution for a movies application that performs well and looks great!

### Who do I talk to? ###

My name: Danny Damsky 

E-mail: dannydamsky99@gmail.com

LinkedIn: https://www.linkedin.com/in/danny-damsky/

### LICENSE ###

   Copyright 2018 Danny Damsky

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.